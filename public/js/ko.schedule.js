
$(function(){
    base_url = 'http://olivopablo.dev/';
    
    ///element
    schedule = function() {
        this.id = ko.observable(),
        this.name = ko.observable(),
        this.description = ko.observable(),
        this.date_happen_confirm = ko.observable(),
        this.date_happen = ko.observable(),
        this.date_due = ko.observable()
        this.datetime = ko.observable()
    }
        
    
    viewModel = function() {
        
        //set local variable
        var _left_content = ko.mapping.fromJS([]);
        var _right_content = ko.observableArray([]);
        var _aside = ko.observableArray([]);
        
        
        //set global to use externally
        this.aside = _aside;
        this.right_content = _right_content;
        this.left_content = _left_content;

      
        updateList= function($data){
            _right_content.removeAll();
            //attach
            $data.date_happen_confirm = ko.observableArray([]);
            _right_content.push($data);
        };
        
        saveList= function($data){
            //ADD new condition
            if(_left_content.indexOf($data)=='-1'){
                _right_content.remove($data);
                $.ajax({
                    type : 'POST',
                    url : base_url+'ajax/admin/schedule/save/?return=json',
                    data : $data
                }).done(function(data){
                    var schedule = ko.mapping.fromJS(data);
                    _left_content.push(schedule);
                })
            }
            //EDIT condition
            else{
                $.ajax({
                    type : 'POST',
                    url : base_url+'ajax/admin/schedule/save',
                    data : ko.toJS($data)
                }) 
            }                        
        }
        
     
        addNew = function($data){
            _aside.removeAll();
            _aside.push(new schedule());
        }
        
        
        
        
        
        publishThis= function($data){  
            if($data.publish()==0){
                $data.publish(1)
                
            }
            else{
                $data.publish(0)
            }
            $data.product_id = $data.id;
            $data.action = 'product';
            $.ajax({
                type : 'POST',
                url : self.base_url+'ajax/admin/',
                data : $data
            })
        };
        
        getPics = function($data){
            this.images = ko.observable(); 
        }
        
        deleteList= function($data){
            self.products.remove($data)
        };
        
    }
    
    ayamarab = new viewModel();
    ko.applyBindings(ayamarab);
    
    $.getJSON('http://olivopablo.dev/ajax/json', {
        action:'allSchedule'
    }, function(data){
        ko.mapping.fromJS(data, ayamarab.left_content);
    });
    
    
    $('select[name=date_month]').on('change',function(){
        var _date= $(this).val();

        $.ajax('http://olivopablo.dev/ajax/json', {
            type : 'get',
            data : {
                action : 'allSchedule',
                date_month : _date
            },
            success : function(data){
                ko.mapping.fromJS(data, ayamarab.left_content)
            }
        });
  
    })



    
})




