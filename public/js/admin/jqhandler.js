// JavaScript Document
server_name_ajax = "http://iphotography.dev/ajax/";
server_name = "http://iphotography.dev/";
var bottompanel = $('#bottom-panel');
var loadingImg = '<h1>loading</h1>';
var status = $('#bottom-panel').append(loadingImg);
bottompanel.hide();

$(function () {

    
    if(jQuery().colorbox){
        $(".group1").colorbox({
            rel:'group1',
            maxHeight:'100%',
            opacity:'0.9'
        });
        
        $(".popup").colorbox({
            maxHeight:'100%',
            opacity:'0.9',
            onComplete:function(){
                var ajaxform = $('#ajax-form')
                if(ajaxform.length!=0){
                    ajaxform.ajaxForm(
                    {
                        dataType:'html',
                        success :function(data){   
                            response = $(data);
                            $('#form_notification').empty().prepend(response.filter('div.alert-message')).show(900).delay(1500).hide(600);
                        }
                    });
                }
            }
            
        });
    }
    
  
    
    if(jQuery().wysiwyg){
        $('#wysiwyg').wysiwyg({
            resizeOptions: {},
            controls: {
                html: {
                    visible: true
                }
            }
        });   
        
    }

    var loginform =  $('#loginform');
    if(loginform.length!=0){
        loginform.jCryption();
    }
    
    var body = $("body");

    /*COMMON PAGE-*---------------------------------------------------*/

    $('.back-button').click(function (){
        parent.history.back();
        return false;
    });       


    /*ajax send for COMMENT AND GRID-BOX CONTENT*/
    var ajaxform = $('.ajax-form');
    
   
    if(ajaxform.length!==0){
        
        ajaxform.ajaxForm(
        {
            dataType:'html',
            success :function(data){   
                response = $(data);
                //filter data. get div for comment and div for reply
                response.filter('.pics-collate').prependTo('#pics-list');
                $('#form_notification').empty().prepend(response.filter('div.alert-message')).show(900).delay(1500).hide(600);
                if(!ajaxform.find('input[name=product_id]')||!ajaxform.find('input[name=post_id]')){
                    ajaxform.find('textarea').val(""); 
                    ajaxform.find('input[type=text]').val("");      
                    
                }
                                    
            }
        });
    }
    



    var lazyload = $('.lazyload');
    if(lazyload.length!=0){
        lazyload.each(function(){
            var eachlazyload =$(this);

            $.ajax({
                type:"POST",
                url :server_name_ajax+'lazyload/', 
                data:eachlazyload.data(),
                success : function (response){
                    eachlazyload.append(response);
                }
            });    
           
        })     
    }
    
    var lazyload_img = $('.lazyload_img');
    
    if(lazyload_img.length!=0){
        lazyload_img.each(function(){
            var lazyload_img =$(this);
            lazyload_img.attr('src',lazyload_img.data['thumb_name']);
        })     
    }
    
    
    var pics_add = $('.pics_add');

    pics_add.live('click',function(e){
        e.preventDefault();
        var pics_add = $(this);
        $.ajax({
            type:"POST",
            url :server_name_ajax+'admin/', 
            data:pics_add.data(),
            success : function (response){
                pics_add.after(response);
            }
        })
    })     
    
    
    /*ajax send for FORUM*/
   
    if($('#ajax-form-forum').length!=0){
        $('#ajax-form-forum').ajaxForm({
            dataType:'html',
            success :function(data){   
                response = $(data);
                //filter data. get div for comment and div for reply
                response.filter('div.grid-box-content').insertBefore('#ajax-form-forum');
                bottompanel.empty().prepend(response.filter('div.alert-message')).show(900).delay(900).hide(600);
                $('#ajax-form-forum').find('textarea').val("");       
            }
        });
    }
    
    /*SIGNUP*/

    $('.verify').blur(function(){
        verify =$(this);
        if($(this).attr('id')=='password2'){
            password = $('#password').val();
            password2 = $(this).val() ;   
            
            if(verify.prev().find('.label')!=null){
                verify.prev().find('.label').remove();
            }
                
            if(password != password2){
                data = '<div class="label label-important">Not match</div>';
            }else{
                data = '<div class="label label-success">match</div>';
            }
            verify.prev().append(data);
        }else{
            $.ajax({
                type: "POST",
                url: server_name_ajax,
                data:{
                    no_auth:$(this).attr('id'),
                    value:$(this).val()
                },
                dataType:'html',
                success : function(data){
                
                
                    if(verify.prev().find('.label')!=null){
                        verify.prev().find('.label').remove();
                    }
                    verify.prev().append(data);
                    if(data==""){
                        verify.prev().find('.label').remove();
                    }
                }
            })  
            
            
        }


    

    }
    )
    
    if($('#ajax-form-editmember').length!=0){
        $('#ajax-form-editmember').ajaxForm({
            target : '#ajax-response-editmember'
        });
    }


    /*normal ajax form send*/
    ajaxFormNormal = '#ajax-form-normal';
    if($(ajaxFormNormal).length > 0){
        $(ajaxFormNormal).ajaxForm({
            target : ajaxFormNormal + ' #ajax-response'
        });
    } 

 
        

    

});
