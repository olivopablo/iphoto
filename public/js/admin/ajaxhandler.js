//ajaxhandler------------------------------------------------
function saveComment(act,pid){
          
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:{
            a: act,
            target: "comment",
            form_token : token,
            
            pid:pid,
            komentar:komentarForm.val()
        },
        success: function(data){
            $('#Comments')
            .prepend(data);
            
            komentarForm
            .val("");
            
            $('#ajax-message-comment',addcommentid)
            .remove();
            addcommentid
            .prepend("<div id='ajax-message-comment' class='alert-message success'><p>Komentar ditambahkan</p></div>")
            
        }
    });       
}


function sendAjax(options){
          
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options ,
        beforeSend : function(){
            bottompanel.show(300).hide(600)
        },
        success: function(data){
            $.colorbox({
                html:data,
                width:'800px',
                opacity:'0.5',
                close:"<a class='btn danger small'>close</a>"
            });
            
            //init ajax-form
            ajaxForm = '#ajax-form';
            if($(ajaxForm).length > 0){
                $(ajaxForm).ajaxForm({
                    target : '#ajax-response'
                });
            }             
         
            
                          
            //init nicEditor
            if($('#nicedit2').length > 0){
                new nicEditor({
                    fullPanel : true
                })
                .panelInstance('nicedit2',{
                    hasPanel : true
                });
            }//if nicedit
        
        
        }
    });       
}



function sendAjax_url(options){
          
    $.ajax({
        type: "POST",
        url: options.url,
        beforeSend : function(){
            bottompanel.show(300).hide(600)
        },
        success: function(data){
            $.colorbox({
                html:data,
                width:'800px',
                opacity:'0.5',
                close:"<a class='btn danger small'>close</a>"
            });
            
            //init ajax-form
            ajaxForm = '#ajax-form';
            if($(ajaxForm).length > 0){
                $(ajaxForm).ajaxForm({
                    target : '#ajax-response'
                });
            }             
         
            
                          
            //init nicEditor
            if($('#nicedit2').length > 0){
                new nicEditor({
                    fullPanel : true
                })
                .panelInstance('nicedit2',{
                    hasPanel : true
                });
            }//if nicedit
        
        
        }
    });       
}

function sendAjaxComment(options){
          
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options,
        beforeSend : function(){
            bottompanel.show(600).delay(900).hide(600)
        },
        success: function(data){
            $.colorbox({
                html:data,
                width:'auto',
                opacity:'0.5'
            });
            
            //init ajax-form
            $('#ajax-form-reply').ajaxForm({
                dataType:'html',   
                success :function(data){   
                    response = $(data);
                    //insert before next parent
                    response.filter('div.grid-box-reply').appendTo('#'+options.parent);
                    $.colorbox.close()
                    bottompanel.empty().prepend(response.filter('div.alert-message')).show(600).delay(900).hide(600)
                }
                
        
        
            }); //ajaxForm
           
        }//success:       
    })//$ajax
}//function



ajaxFormGalleryId = $('#ajax-form-gallery');
ajaxResponseId = $('#ajax-response');

if(ajaxFormGalleryId.length!=0){
    ajaxFormGalleryId.ajaxForm({
        target : '#ajax-response', 
        success:  ajaxFormCallback                      
    });
}
    
function ajaxFormCallback(){
    ajaxFormGalleryId.find('input[type=file]').val('');
    getAddedItems(); 
}
    
function getAddedItems(){
    var targetId= $('#id-image-gallery');
    
    var options = {
        target: "images",
        get: "addedImages",
        form_token : token
    };
       
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options,
        success: function(data){            
            targetId.prepend(data);

        }
    });         
}

function deleteProduct(pid){
    var options = {
        target: "product",
        get: "delete",
        form_token : token,
        pid:pid
    }
    var callback = $('#product-'+pid).remove();
    
    sendAjax_panel(options,callback);
}

function sendAjax_panel(options,callback){
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options,
        success: function(data){            
            callback
        }
    });  
}

function sendAjax_cart(options){
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options,
        success: function(data){            
            $('#'+ options.id).after(data);
        }
    });  
}





/*function etc*/

$("add-comment").click(function() {

    var message_wall = $('#message_wall').attr('value');

    $.ajax({
        type: "POST",
        url: "../../ajax/ajaxtest.php",
        data:{
            a: 1,
            form_token : token
        },
        success: function(){
            $("#addCommentId").fadeIn();
            $("#addCommentId").prepend("<li>errr</li>");
        }
    });
    return false;
});


/*init*/



function sendAjax_slidedown(options,appendTo){
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options,
        beforeSend : function(){
            bottompanel.show('slow');
        },
        success: function(data){            
             
             if(appendTo==null){
               appendTo = "#list-detail";  
             }
             
            $(appendTo).empty().append(data);        
            
            if($('#ajax-form').length > 0){
                
            
            $('#ajax-form').ajaxForm({
                target : '#ajax-response'
            });
            
            }
            
            
            ajaxFormImages = '#ajax-images';
            
            //EDIT PRODUCT IMAGES DASHBOARD
            //if #ajax-images exists bind ajaxForm
            if($(ajaxFormImages).length > 0){
                $(ajaxFormImages).ajaxForm({
                    dataType:'html',   
                    success: function(data){
                        response = $(data);
                        //filter data response by ID
                        returnData = response.filter('#response');
                        alertMessage = response.filter('.alert-message');
                                
                                
                                
                        //attach filtereddata   
                        //if image-product-holder empty use div from id-img-gallery
                        imageholder ='#image-product-holder';
                        
                         if($(imageholder).length==0){
                                imageholder = '#id-image-gallery';
                            }
                            
                        $(imageholder).append(returnData);  
                        
                        $('#ajax-response-images').empty().append(alertMessage); 
                
                        //empty file input
                        $(ajaxFormImages+' input[type=file]').val("")
                        
                        
                        //init nicEditor
            
                        
                    }//success(data) #ajax-images
                });
        
            }//#ajax-images
            
            if($('#nicedit2').length > 0){
                new nicEditor({
                    fullPanel : true
                })
                .panelInstance('nicedit2',{
                    hasPanel : true
                });
            }//if nicedit
            
            bottompanel.delay(600).hide('slow');
        }//success data
        
        
    });  
}

function sendAjax_more(options,callback){
    
    $.ajax({
        type: "POST",
        url: server_name_ajax,
        data:options,
        success: function(data){        
            if(data!=""){
                $(data).insertAfter(callback);       
            }
        }

    });  
    
}


