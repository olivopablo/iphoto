base_url = 'http://iphotography.dev/';

$(function(){ 
    if(jQuery().colorbox){
        $(".group1").colorbox({
            rel:'group1',
            maxHeight:'100%',
            opacity:'0.9'
        });
    }
    if(jQuery().revolver){
        $('.slider').revolver({
            transition: {
                type: 'slide',
                speed:'3000'
            }
        });
    
    }
        
        
    var lazyload_img = $('.lazyload_img');
    
    if(lazyload_img.length!=0){
        lazyload_img.each(function(){
            var lazyload_img =$(this);
            lazyload_img.attr('src',base_url+'public/user/images/'+lazyload_img.data('thumb_name'));
        })     
    }
    
    if(jQuery().masonry){
        $('#masonry').masonry({
            itemSelector: '.child',
            columnWidth: function( containerWidth ) {
                return containerWidth / 20;
            }
        });
    }
    
    $('nav ul li').on('click',function(){
        alink = $(this).find('a').attr('href');
        window.location = alink;
    })
    
    
    var hoverNclickButton = 'nav ul li';
    var body = $('body');
    $(hoverNclickButton).hover(       
        function(){
            body.css('cursor','pointer');
            $(this).addClass('hover-blue')
        },
        function(){
            body.css('cursor','auto'),
            $(this).removeClass('hover-blue')
        }
        )
            

        
    button = function(){
        
        var thumb_size = {
            small : '150px',
            normal: '408px',
            big : '600px'
        };
        
        this.thumb = function(size,selector){
            $(selector.toString()).css('width',thumb_size[size]);
        }
    }


    btn = new button();
    
    // div on admin/update
    affected_sizing = ['.pics-collate','.lazyload_img','.pics-collate .caption .action'];
    
    $('a.small_thumb').on('click',function(){ 
        btn.thumb('small',affected_sizing)
    })
    $('a.normal_thumb').on('click',function(){ 
        btn.thumb('normal',affected_sizing)
    })
    $('a.big_thumb').on('click',function(){ 
        btn.thumb('big',affected_sizing)
    })
    
    
        
        
      
})


