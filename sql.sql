CREATE DATABASE  IF NOT EXISTS `db_local` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_local`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS users_old;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE users_old (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  `mode` varchar(5) DEFAULT NULL,
  access_level int(1) NOT NULL,
  admin enum('yes','no') DEFAULT 'no',
  email varchar(60) DEFAULT NULL,
  gambar varchar(100) DEFAULT NULL,
  fullname varchar(80) DEFAULT NULL,
  location varchar(100) DEFAULT NULL,
  address varchar(120) DEFAULT NULL,
  telepon varchar(30) DEFAULT NULL,
  tempat_lahir varchar(60) DEFAULT NULL,
  tgl_lahir varchar(12) DEFAULT NULL,
  date_register date DEFAULT NULL,
  ipaddress varchar(20) DEFAULT NULL,
  hostname varchar(120) DEFAULT NULL,
  note text,
  last_login date DEFAULT NULL,
  `session` varchar(60) DEFAULT NULL,
  `status` int(15) NOT NULL,
  meta text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES users_old WRITE;
/*!40000 ALTER TABLE users_old DISABLE KEYS */;
/*!40000 ALTER TABLE users_old ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE users (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  ip_address varchar(45) NOT NULL,
  username varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  salt varchar(40) DEFAULT NULL,
  email varchar(100) NOT NULL,
  activation_code varchar(40) DEFAULT NULL,
  forgotten_password_code varchar(40) DEFAULT NULL,
  remember_code varchar(40) DEFAULT NULL,
  created_on int(11) unsigned NOT NULL,
  last_login int(11) unsigned DEFAULT NULL,
  active tinyint(1) unsigned DEFAULT NULL,
  first_name varchar(50) DEFAULT NULL,
  last_name varchar(50) DEFAULT NULL,
  company varchar(100) DEFAULT NULL,
  phone varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES users WRITE;
/*!40000 ALTER TABLE users DISABLE KEYS */;
INSERT INTO users (id, ip_address, username, password, salt, email, activation_code, forgotten_password_code, remember_code, created_on, last_login, active, first_name, last_name, company, phone) VALUES (1,'2130706433','administrator','93498f3def61a8daf34839cc6d7045117f051828','9462e8eee0','admin@admin.com','',NULL,'be87c428ce42d7bae08c5c99109150d7115240b5',1268889823,1341324558,1,'Admin','istrator','ADMIN','0'),(2,'\n\0','emon emon','02559bfab769e84a005e795ab56a4d5ffc510f5f',NULL,'emon@dreamy.com',NULL,NULL,NULL,1341323339,1341323339,1,'emon','emon','emon','08112533--');
/*!40000 ALTER TABLE users ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS schedule;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  description text,
  `datetime` datetime DEFAULT NULL,
  date_happen datetime DEFAULT NULL,
  date_due datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES schedule WRITE;
/*!40000 ALTER TABLE schedule DISABLE KEYS */;
INSERT INTO schedule (id, name, description, datetime, date_happen, date_due) VALUES (1,'cruttttt','husaaaa','2012-06-17 06:26:18','0002-02-02 02:02:00','0002-02-02 02:02:00'),(2,'weezer','<p>meong meong<br></p>','2012-06-17 06:30:18','0002-02-02 02:02:00','0000-00-00 00:00:00'),(3,'wee','meong meong uhuk','2012-06-17 06:36:43','0002-02-02 02:02:00','0000-00-00 00:00:00'),(4,'wee','<p>meong meong<br></p>','2012-06-17 06:38:43','0002-02-02 02:02:00','0002-02-02 02:02:00'),(5,'edy','<p>prewedding meong<br></p>','2012-06-17 06:38:56','0002-02-02 02:02:00','0000-00-00 00:00:00'),(6,'edyd','<p>prewedding meong<br></p>','2012-06-17 06:40:47','0002-02-02 02:02:00','0000-00-00 00:00:00'),(7,'edydddd','Is kebes salastia7777','2012-06-17 08:48:26','2012-06-27 19:00:00',NULL),(8,'edy5ffffff','<p>prewedding meong<br></p>fffffffguuuu','2012-06-17 07:03:54','2012-06-27 19:00:00','0000-00-00 00:00:00'),(9,'edy78','<p>prewedding meong<br></p>8666','2012-06-17 18:46:43','2012-05-16 19:00:00',NULL),(10,'edyi','<p>prewedding meong<br></p>','2012-06-17 10:13:24','2012-06-27 19:00:00',NULL),(11,'edy','<p>prewedding meong<br></p>','2012-06-16 21:56:03','2012-05-23 19:00:00',NULL),(12,'edydddd','Is kebes salastiafgfg','2012-06-17 06:51:00',NULL,NULL),(13,'undefined','undefined','2012-06-17 10:40:19',NULL,NULL),(14,'asdf','asdf','2012-06-19 02:00:03',NULL,NULL),(15,'asdf','asdf','2012-06-19 02:01:04',NULL,NULL),(16,'asdf','asdf','2012-06-19 02:01:17',NULL,NULL),(17,'undefined','uy','2012-06-19 02:02:25',NULL,NULL),(18,'asdf','sdf','2012-06-19 02:03:54',NULL,NULL),(19,'asdfsdf','sdfasdf','2012-06-19 02:04:37',NULL,NULL),(20,'asdf','asdf','2012-06-19 02:07:56',NULL,NULL),(21,'sdf','undefined','2012-06-19 02:11:22',NULL,NULL),(22,'sdf','567','2012-06-19 03:30:19',NULL,NULL),(23,'sdf','567uuuu','2012-06-19 04:33:27',NULL,NULL),(24,'edy5ffffff','<p>prewedding meong<br></p>fffffffguuuu9999','2012-06-19 04:34:06',NULL,NULL),(25,'sdf456456','5674565','2012-06-19 04:37:15',NULL,NULL),(26,'asdf','asdf','2012-06-19 04:39:18',NULL,NULL),(27,'aaa','undefined','2012-06-19 04:40:13',NULL,NULL),(28,'undefined','sdf','2012-06-19 04:43:12',NULL,NULL),(29,'undefined','sdf','2012-06-19 04:44:05',NULL,NULL),(30,'sdf','sdf','2012-06-19 04:45:09',NULL,NULL),(31,'sdfsdd','sdfddddddd','2012-06-19 04:45:45',NULL,NULL),(32,'gggg','ggggg','2012-06-19 04:51:33',NULL,NULL),(33,NULL,NULL,'2012-06-19 04:51:52',NULL,NULL),(34,'gggg','ggggg','2012-06-19 04:59:19',NULL,NULL),(35,'ddd','ddddddd','2012-06-19 04:59:33',NULL,NULL),(36,'ddd6888','ddddddd666','2012-06-19 05:05:42',NULL,NULL),(37,'677','777','2012-06-19 05:06:13',NULL,NULL),(38,'677','7776666','2012-06-19 05:06:27',NULL,NULL),(39,'67799','7776666','2012-06-19 05:06:49',NULL,NULL),(40,'edyddddhehehe','Is kebes salastia7777','2012-06-19 05:07:11',NULL,NULL),(41,'edy78k','<p>prewedding meong<br></p>8666','2012-06-19 05:07:30',NULL,NULL),(42,'677666666666666666333333333','777dddd','2012-06-19 05:11:24',NULL,NULL),(43,'677666666666666666','777dddd','2012-06-19 05:08:22',NULL,NULL),(44,'677ssssddddddtttttttttt','777dddd','2012-06-19 05:11:06',NULL,NULL);
/*!40000 ALTER TABLE schedule ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS users_groups;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE users_groups (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  user_id mediumint(8) unsigned NOT NULL,
  group_id mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES users_groups WRITE;
/*!40000 ALTER TABLE users_groups DISABLE KEYS */;
INSERT INTO users_groups (id, user_id, group_id) VALUES (1,1,1),(2,1,2),(3,2,2);
/*!40000 ALTER TABLE users_groups ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS product_categories;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE product_categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES product_categories WRITE;
/*!40000 ALTER TABLE product_categories DISABLE KEYS */;
INSERT INTO product_categories (id, name) VALUES (1,'Recent'),(2,'Featured'),(3,'Mens'),(4,'Girls');
/*!40000 ALTER TABLE product_categories ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS images;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE images (
  id int(11) NOT NULL AUTO_INCREMENT,
  label varchar(100) DEFAULT NULL,
  description text,
  `date` datetime DEFAULT NULL,
  publish varchar(45) DEFAULT NULL,
  meta text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES images WRITE;
/*!40000 ALTER TABLE images DISABLE KEYS */;
INSERT INTO images (id, label, description, date, publish, meta) VALUES (71,'asdfdf','asdfdf','2012-05-26 12:37:10','0','a:15:{s:9:\"file_name\";s:6:\"06.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:60:\"/media/sf_Website_Folder/Portfolio/public/user/images/06.jpg\";s:8:\"raw_name\";s:2:\"06\";s:9:\"orig_name\";s:6:\"06.jpg\";s:11:\"client_name\";s:6:\"06.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:52.149999999999999;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";s:10:\"thumb_name\";s:12:\"06_thumb.jpg\";}'),(73,'-','=','2012-05-26 12:40:51','0','a:15:{s:9:\"file_name\";s:7:\"201.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:61:\"/media/sf_Website_Folder/Portfolio/public/user/images/201.jpg\";s:8:\"raw_name\";s:3:\"201\";s:9:\"orig_name\";s:6:\"20.jpg\";s:11:\"client_name\";s:6:\"20.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:49.140000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";s:10:\"thumb_name\";s:13:\"201_thumb.jpg\";}'),(78,'---','----','2012-05-26 12:50:25','0','a:15:{s:9:\"file_name\";s:11:\"07_(2)3.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:65:\"/media/sf_Website_Folder/Portfolio/public/user/images/07_(2)3.jpg\";s:8:\"raw_name\";s:7:\"07_(2)3\";s:9:\"orig_name\";s:10:\"07_(2).jpg\";s:11:\"client_name\";s:10:\"07 (2).jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:75.420000000000002;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";s:10:\"thumb_name\";s:17:\"07_(2)3_thumb.jpg\";}'),(82,'5676','66666','2012-05-26 13:16:30','0','a:15:{s:9:\"file_name\";s:10:\"19_(2).jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:64:\"/media/sf_Website_Folder/Portfolio/public/user/images/19_(2).jpg\";s:8:\"raw_name\";s:6:\"19_(2)\";s:9:\"orig_name\";s:10:\"19_(2).jpg\";s:11:\"client_name\";s:10:\"19 (2).jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:88.489999999999995;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";s:10:\"thumb_name\";s:16:\"19_(2)_thumb.jpg\";}'),(83,'7','9','2012-05-26 13:18:16','0','a:15:{s:9:\"file_name\";s:7:\"313.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:61:\"/media/sf_Website_Folder/Portfolio/public/user/images/313.jpg\";s:8:\"raw_name\";s:3:\"313\";s:9:\"orig_name\";s:6:\"31.jpg\";s:11:\"client_name\";s:6:\"31.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:95.120000000000005;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";s:10:\"thumb_name\";s:13:\"313_thumb.jpg\";}'),(84,'satuapat','SatuAtap Company','2012-05-26 13:20:45','0','a:15:{s:9:\"file_name\";s:42:\"company_profile_SATU_ATAP_by_aremanvin.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:96:\"/media/sf_Website_Folder/Portfolio/public/user/images/company_profile_SATU_ATAP_by_aremanvin.jpg\";s:8:\"raw_name\";s:38:\"company_profile_SATU_ATAP_by_aremanvin\";s:9:\"orig_name\";s:42:\"company_profile_SATU_ATAP_by_aremanvin.jpg\";s:11:\"client_name\";s:42:\"company_profile_SATU_ATAP_by_aremanvin.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:516.75999999999999;s:8:\"is_image\";b:1;s:11:\"image_width\";i:579;s:12:\"image_height\";i:819;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"579\" height=\"819\"\";s:10:\"thumb_name\";s:48:\"company_profile_SATU_ATAP_by_aremanvin_thumb.jpg\";}'),(85,'label','descriptpe','2012-05-26 13:45:02','0','a:15:{s:9:\"file_name\";s:6:\"26.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:60:\"/media/sf_Website_Folder/Portfolio/public/user/images/26.jpg\";s:8:\"raw_name\";s:2:\"26\";s:9:\"orig_name\";s:6:\"26.jpg\";s:11:\"client_name\";s:6:\"26.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:64;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";s:10:\"thumb_name\";s:12:\"26_thumb.jpg\";}'),(100,'label ','description','2012-05-26 21:29:03','0','a:14:{s:9:\"file_name\";s:10:\"12_(2).jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:64:\"/media/sf_Website_Folder/Portfolio/public/user/images/12_(2).jpg\";s:8:\"raw_name\";s:6:\"12_(2)\";s:9:\"orig_name\";s:10:\"12_(2).jpg\";s:11:\"client_name\";s:10:\"12 (2).jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:64.060000000000002;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";}'),(101,'2','2','2012-05-26 21:29:51','0','a:14:{s:9:\"file_name\";s:6:\"04.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:60:\"/media/sf_Website_Folder/Portfolio/public/user/images/04.jpg\";s:8:\"raw_name\";s:2:\"04\";s:9:\"orig_name\";s:6:\"04.jpg\";s:11:\"client_name\";s:6:\"04.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:109.41;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";}'),(102,'2','2','2012-05-26 21:33:00','0','a:14:{s:9:\"file_name\";s:7:\"041.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:61:\"/media/sf_Website_Folder/Portfolio/public/user/images/041.jpg\";s:8:\"raw_name\";s:3:\"041\";s:9:\"orig_name\";s:6:\"04.jpg\";s:11:\"client_name\";s:6:\"04.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:109.41;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"300\"\";}'),(107,'','','2012-05-27 15:15:33','0','a:15:{s:9:\"file_name\";s:25:\"lentil_44c4e6bf214ec3.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:79:\"/media/sf_Website_Folder/Portfolio/public/user/images/lentil_44c4e6bf214ec3.jpg\";s:8:\"raw_name\";s:21:\"lentil_44c4e6bf214ec3\";s:9:\"orig_name\";s:24:\"lentil_44c4e6bf214ec.jpg\";s:11:\"client_name\";s:24:\"lentil_44c4e6bf214ec.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:63.969999999999999;s:8:\"is_image\";b:1;s:11:\"image_width\";i:298;s:12:\"image_height\";i:450;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"298\" height=\"450\"\";s:10:\"thumb_name\";s:31:\"lentil_44c4e6bf214ec3_thumb.jpg\";}'),(110,'','','2012-05-28 12:43:08','0','a:15:{s:9:\"file_name\";s:61:\"534655_284928614920053_100002087504585_651651_859588421_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:115:\"/media/sf_Website_Folder/Portfolio/public/user/images/534655_284928614920053_100002087504585_651651_859588421_n.jpg\";s:8:\"raw_name\";s:57:\"534655_284928614920053_100002087504585_651651_859588421_n\";s:9:\"orig_name\";s:61:\"534655_284928614920053_100002087504585_651651_859588421_n.jpg\";s:11:\"client_name\";s:61:\"534655_284928614920053_100002087504585_651651_859588421_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:61.689999999999998;s:8:\"is_image\";b:1;s:11:\"image_width\";i:500;s:12:\"image_height\";i:752;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"500\" height=\"752\"\";s:10:\"thumb_name\";s:67:\"534655_284928614920053_100002087504585_651651_859588421_n_thumb.jpg\";}'),(111,'','','2012-05-28 12:43:15','0','a:15:{s:9:\"file_name\";s:36:\"260222_130773157002267_8091050_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:90:\"/media/sf_Website_Folder/Portfolio/public/user/images/260222_130773157002267_8091050_n.jpg\";s:8:\"raw_name\";s:32:\"260222_130773157002267_8091050_n\";s:9:\"orig_name\";s:36:\"260222_130773157002267_8091050_n.jpg\";s:11:\"client_name\";s:36:\"260222_130773157002267_8091050_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:160.66;s:8:\"is_image\";b:1;s:11:\"image_width\";i:465;s:12:\"image_height\";i:700;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"465\" height=\"700\"\";s:10:\"thumb_name\";s:42:\"260222_130773157002267_8091050_n_thumb.jpg\";}'),(112,'','','2012-05-28 12:43:23','0','a:15:{s:9:\"file_name\";s:59:\"270108_133248370088079_100002087504585_249133_4938642_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:113:\"/media/sf_Website_Folder/Portfolio/public/user/images/270108_133248370088079_100002087504585_249133_4938642_n.jpg\";s:8:\"raw_name\";s:55:\"270108_133248370088079_100002087504585_249133_4938642_n\";s:9:\"orig_name\";s:59:\"270108_133248370088079_100002087504585_249133_4938642_n.jpg\";s:11:\"client_name\";s:59:\"270108_133248370088079_100002087504585_249133_4938642_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:62.479999999999997;s:8:\"is_image\";b:1;s:11:\"image_width\";i:720;s:12:\"image_height\";i:479;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"720\" height=\"479\"\";s:10:\"thumb_name\";s:65:\"270108_133248370088079_100002087504585_249133_4938642_n_thumb.jpg\";}'),(113,'','','2012-05-28 12:44:43','0','a:15:{s:9:\"file_name\";s:59:\"284197_141402859272630_100002087504585_279183_4054886_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:113:\"/media/sf_Website_Folder/Portfolio/public/user/images/284197_141402859272630_100002087504585_279183_4054886_n.jpg\";s:8:\"raw_name\";s:55:\"284197_141402859272630_100002087504585_279183_4054886_n\";s:9:\"orig_name\";s:59:\"284197_141402859272630_100002087504585_279183_4054886_n.jpg\";s:11:\"client_name\";s:59:\"284197_141402859272630_100002087504585_279183_4054886_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:86.409999999999997;s:8:\"is_image\";b:1;s:11:\"image_width\";i:479;s:12:\"image_height\";i:720;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"479\" height=\"720\"\";s:10:\"thumb_name\";s:65:\"284197_141402859272630_100002087504585_279183_4054886_n_thumb.jpg\";}'),(115,'','','2012-05-28 14:09:16','0','a:15:{s:9:\"file_name\";s:59:\"263513_135069129906003_100002087504585_259018_4307071_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:113:\"/media/sf_Website_Folder/Portfolio/public/user/images/263513_135069129906003_100002087504585_259018_4307071_n.jpg\";s:8:\"raw_name\";s:55:\"263513_135069129906003_100002087504585_259018_4307071_n\";s:9:\"orig_name\";s:59:\"263513_135069129906003_100002087504585_259018_4307071_n.jpg\";s:11:\"client_name\";s:59:\"263513_135069129906003_100002087504585_259018_4307071_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:45.780000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:720;s:12:\"image_height\";i:479;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"720\" height=\"479\"\";s:10:\"thumb_name\";s:65:\"263513_135069129906003_100002087504585_259018_4307071_n_thumb.jpg\";}'),(116,'','','2012-05-28 18:51:57','0','a:15:{s:9:\"file_name\";s:59:\"261750_138183742927875_100002087504585_269412_7872618_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:113:\"/media/sf_Website_Folder/Portfolio/public/user/images/261750_138183742927875_100002087504585_269412_7872618_n.jpg\";s:8:\"raw_name\";s:55:\"261750_138183742927875_100002087504585_269412_7872618_n\";s:9:\"orig_name\";s:59:\"261750_138183742927875_100002087504585_269412_7872618_n.jpg\";s:11:\"client_name\";s:59:\"261750_138183742927875_100002087504585_269412_7872618_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:59.43;s:8:\"is_image\";b:1;s:11:\"image_width\";i:720;s:12:\"image_height\";i:479;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"720\" height=\"479\"\";s:10:\"thumb_name\";s:65:\"261750_138183742927875_100002087504585_269412_7872618_n_thumb.jpg\";}'),(119,'','','2012-05-28 21:47:00','0','a:15:{s:9:\"file_name\";s:5:\"5.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:59:\"/media/sf_Website_Folder/Portfolio/public/user/images/5.jpg\";s:8:\"raw_name\";s:1:\"5\";s:9:\"orig_name\";s:5:\"5.jpg\";s:11:\"client_name\";s:5:\"5.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:107.93000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:450;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"450\" height=\"300\"\";s:10:\"thumb_name\";s:11:\"5_thumb.jpg\";}'),(120,'','','2012-05-28 21:47:35','0','a:15:{s:9:\"file_name\";s:10:\"1111-3.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:64:\"/media/sf_Website_Folder/Portfolio/public/user/images/1111-3.jpg\";s:8:\"raw_name\";s:6:\"1111-3\";s:9:\"orig_name\";s:10:\"1111-3.jpg\";s:11:\"client_name\";s:10:\"1111-3.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:466.47000000000003;s:8:\"is_image\";b:1;s:11:\"image_width\";i:900;s:12:\"image_height\";i:600;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"900\" height=\"600\"\";s:10:\"thumb_name\";s:16:\"1111-3_thumb.jpg\";}'),(121,'','','2012-05-28 21:47:44','0','a:15:{s:9:\"file_name\";s:8:\"1114.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:62:\"/media/sf_Website_Folder/Portfolio/public/user/images/1114.jpg\";s:8:\"raw_name\";s:4:\"1114\";s:9:\"orig_name\";s:8:\"1114.jpg\";s:11:\"client_name\";s:8:\"1114.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:178.55000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:900;s:12:\"image_height\";i:600;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"900\" height=\"600\"\";s:10:\"thumb_name\";s:14:\"1114_thumb.jpg\";}'),(122,'','','2012-05-28 21:47:56','0','a:15:{s:9:\"file_name\";s:18:\"587_edith_zico.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:72:\"/media/sf_Website_Folder/Portfolio/public/user/images/587_edith_zico.jpg\";s:8:\"raw_name\";s:14:\"587_edith_zico\";s:9:\"orig_name\";s:18:\"587_edith_zico.jpg\";s:11:\"client_name\";s:18:\"587_edith_zico.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:199.72999999999999;s:8:\"is_image\";b:1;s:11:\"image_width\";i:450;s:12:\"image_height\";i:673;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"450\" height=\"673\"\";s:10:\"thumb_name\";s:24:\"587_edith_zico_thumb.jpg\";}'),(123,'','','2012-05-28 21:48:06','0','a:15:{s:9:\"file_name\";s:7:\"P06.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:61:\"/media/sf_Website_Folder/Portfolio/public/user/images/P06.jpg\";s:8:\"raw_name\";s:3:\"P06\";s:9:\"orig_name\";s:7:\"P06.jpg\";s:11:\"client_name\";s:7:\"P06.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:14.68;s:8:\"is_image\";b:1;s:11:\"image_width\";i:302;s:12:\"image_height\";i:383;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"302\" height=\"383\"\";s:10:\"thumb_name\";s:13:\"P06_thumb.jpg\";}'),(139,'','','2012-06-22 02:01:59','0','a:14:{s:9:\"file_name\";s:35:\"252470_154195284652264_370449_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:89:\"/media/sf_Website_Folder/Portfolio/public/user/images/252470_154195284652264_370449_n.jpg\";s:8:\"raw_name\";s:31:\"252470_154195284652264_370449_n\";s:9:\"orig_name\";s:35:\"252470_154195284652264_370449_n.jpg\";s:11:\"client_name\";s:35:\"252470_154195284652264_370449_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:81.150000000000006;s:8:\"is_image\";b:1;s:11:\"image_width\";i:700;s:12:\"image_height\";i:591;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"700\" height=\"591\"\";}'),(140,'','','2012-06-22 02:02:05','0','a:14:{s:9:\"file_name\";s:35:\"254342_154705437934582_602307_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:89:\"/media/sf_Website_Folder/Portfolio/public/user/images/254342_154705437934582_602307_n.jpg\";s:8:\"raw_name\";s:31:\"254342_154705437934582_602307_n\";s:9:\"orig_name\";s:35:\"254342_154705437934582_602307_n.jpg\";s:11:\"client_name\";s:35:\"254342_154705437934582_602307_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:90.530000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:700;s:12:\"image_height\";i:682;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"700\" height=\"682\"\";}'),(141,'','','2012-06-22 02:02:10','0','a:14:{s:9:\"file_name\";s:35:\"254519_149702801768179_208327_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:89:\"/media/sf_Website_Folder/Portfolio/public/user/images/254519_149702801768179_208327_n.jpg\";s:8:\"raw_name\";s:31:\"254519_149702801768179_208327_n\";s:9:\"orig_name\";s:35:\"254519_149702801768179_208327_n.jpg\";s:11:\"client_name\";s:35:\"254519_149702801768179_208327_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:33.509999999999998;s:8:\"is_image\";b:1;s:11:\"image_width\";i:400;s:12:\"image_height\";i:600;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"400\" height=\"600\"\";}'),(142,'','','2012-06-22 02:02:17','0','a:14:{s:9:\"file_name\";s:36:\"254006_154574337947692_6838844_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:90:\"/media/sf_Website_Folder/Portfolio/public/user/images/254006_154574337947692_6838844_n.jpg\";s:8:\"raw_name\";s:32:\"254006_154574337947692_6838844_n\";s:9:\"orig_name\";s:36:\"254006_154574337947692_6838844_n.jpg\";s:11:\"client_name\";s:36:\"254006_154574337947692_6838844_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:103.27;s:8:\"is_image\";b:1;s:11:\"image_width\";i:700;s:12:\"image_height\";i:618;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"700\" height=\"618\"\";}'),(143,'','','2012-06-22 02:02:27','0','a:14:{s:9:\"file_name\";s:36:\"264609_154706401267819_8159073_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:90:\"/media/sf_Website_Folder/Portfolio/public/user/images/264609_154706401267819_8159073_n.jpg\";s:8:\"raw_name\";s:32:\"264609_154706401267819_8159073_n\";s:9:\"orig_name\";s:36:\"264609_154706401267819_8159073_n.jpg\";s:11:\"client_name\";s:36:\"264609_154706401267819_8159073_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:89.670000000000002;s:8:\"is_image\";b:1;s:11:\"image_width\";i:700;s:12:\"image_height\";i:678;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"700\" height=\"678\"\";}'),(144,'','','2012-06-22 02:02:33','0','a:14:{s:9:\"file_name\";s:35:\"264767_154729677932158_928001_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:89:\"/media/sf_Website_Folder/Portfolio/public/user/images/264767_154729677932158_928001_n.jpg\";s:8:\"raw_name\";s:31:\"264767_154729677932158_928001_n\";s:9:\"orig_name\";s:35:\"264767_154729677932158_928001_n.jpg\";s:11:\"client_name\";s:35:\"264767_154729677932158_928001_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:71.519999999999996;s:8:\"is_image\";b:1;s:11:\"image_width\";i:700;s:12:\"image_height\";i:592;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"700\" height=\"592\"\";}'),(145,'','','2012-06-22 02:06:26','0','a:14:{s:9:\"file_name\";s:36:\"261625_154199287985197_7213883_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:90:\"/media/sf_Website_Folder/Portfolio/public/user/images/261625_154199287985197_7213883_n.jpg\";s:8:\"raw_name\";s:32:\"261625_154199287985197_7213883_n\";s:9:\"orig_name\";s:36:\"261625_154199287985197_7213883_n.jpg\";s:11:\"client_name\";s:36:\"261625_154199287985197_7213883_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:45.100000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:454;s:12:\"image_height\";i:700;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"454\" height=\"700\"\";}'),(146,'','','2012-06-22 02:06:44','0','a:14:{s:9:\"file_name\";s:39:\"399810_241980592540399_1396614541_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:93:\"/media/sf_Website_Folder/Portfolio/public/user/images/399810_241980592540399_1396614541_n.jpg\";s:8:\"raw_name\";s:35:\"399810_241980592540399_1396614541_n\";s:9:\"orig_name\";s:39:\"399810_241980592540399_1396614541_n.jpg\";s:11:\"client_name\";s:39:\"399810_241980592540399_1396614541_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:69.099999999999994;s:8:\"is_image\";b:1;s:11:\"image_width\";i:960;s:12:\"image_height\";i:659;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"960\" height=\"659\"\";}'),(147,'','','2012-06-22 02:06:50','0','a:14:{s:9:\"file_name\";s:9:\"1_(5).jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:63:\"/media/sf_Website_Folder/Portfolio/public/user/images/1_(5).jpg\";s:8:\"raw_name\";s:5:\"1_(5)\";s:9:\"orig_name\";s:9:\"1_(5).jpg\";s:11:\"client_name\";s:9:\"1 (5).jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:61.659999999999997;s:8:\"is_image\";b:1;s:11:\"image_width\";i:720;s:12:\"image_height\";i:392;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"720\" height=\"392\"\";}'),(150,'','','2012-06-22 02:07:15','0','a:14:{s:9:\"file_name\";s:36:\"251664_152197771518682_3877818_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:90:\"/media/sf_Website_Folder/Portfolio/public/user/images/251664_152197771518682_3877818_n.jpg\";s:8:\"raw_name\";s:32:\"251664_152197771518682_3877818_n\";s:9:\"orig_name\";s:36:\"251664_152197771518682_3877818_n.jpg\";s:11:\"client_name\";s:36:\"251664_152197771518682_3877818_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:60.850000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:399;s:12:\"image_height\";i:600;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"399\" height=\"600\"\";}'),(151,'','','2012-06-22 02:07:27','0','a:14:{s:9:\"file_name\";s:38:\"538712_325336454204812_501930218_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:92:\"/media/sf_Website_Folder/Portfolio/public/user/images/538712_325336454204812_501930218_n.jpg\";s:8:\"raw_name\";s:34:\"538712_325336454204812_501930218_n\";s:9:\"orig_name\";s:38:\"538712_325336454204812_501930218_n.jpg\";s:11:\"client_name\";s:38:\"538712_325336454204812_501930218_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:49.520000000000003;s:8:\"is_image\";b:1;s:11:\"image_width\";i:720;s:12:\"image_height\";i:432;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"720\" height=\"432\"\";}'),(152,'Home Page','','2012-06-28 08:52:33','0','a:14:{s:9:\"file_name\";s:37:\"Fullscreen_capture_28062012_82855.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:54:\"/media/sf_Website_Folder/Portfolio/public/user/images/\";s:9:\"full_path\";s:91:\"/media/sf_Website_Folder/Portfolio/public/user/images/Fullscreen_capture_28062012_82855.jpg\";s:8:\"raw_name\";s:33:\"Fullscreen_capture_28062012_82855\";s:9:\"orig_name\";s:37:\"Fullscreen_capture_28062012_82855.jpg\";s:11:\"client_name\";s:37:\"Fullscreen capture 28062012 82855.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:183.59;s:8:\"is_image\";b:1;s:11:\"image_width\";i:1161;s:12:\"image_height\";i:655;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:25:\"width=\"1161\" height=\"655\"\";}'),(157,'','','2012-06-30 06:43:17','0','a:14:{s:9:\"file_name\";s:35:\"47620_109002522504874_7260179_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:92:\"/media/sf_Website_Folder/iphotography/public/user/images/47620_109002522504874_7260179_n.jpg\";s:8:\"raw_name\";s:31:\"47620_109002522504874_7260179_n\";s:9:\"orig_name\";s:35:\"47620_109002522504874_7260179_n.jpg\";s:11:\"client_name\";s:35:\"47620_109002522504874_7260179_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:65.930000000000007;s:8:\"is_image\";b:1;s:11:\"image_width\";i:400;s:12:\"image_height\";i:600;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"400\" height=\"600\"\";}'),(158,'','','2012-06-30 06:43:24','0','a:14:{s:9:\"file_name\";s:36:\"47644_108106855927774_4583984_n1.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:93:\"/media/sf_Website_Folder/iphotography/public/user/images/47644_108106855927774_4583984_n1.jpg\";s:8:\"raw_name\";s:32:\"47644_108106855927774_4583984_n1\";s:9:\"orig_name\";s:35:\"47644_108106855927774_4583984_n.jpg\";s:11:\"client_name\";s:35:\"47644_108106855927774_4583984_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:43.549999999999997;s:8:\"is_image\";b:1;s:11:\"image_width\";i:333;s:12:\"image_height\";i:500;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"333\" height=\"500\"\";}'),(159,'','','2012-06-30 06:43:34','0','a:14:{s:9:\"file_name\";s:35:\"63896_108078572597269_5894492_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:92:\"/media/sf_Website_Folder/iphotography/public/user/images/63896_108078572597269_5894492_n.jpg\";s:8:\"raw_name\";s:31:\"63896_108078572597269_5894492_n\";s:9:\"orig_name\";s:35:\"63896_108078572597269_5894492_n.jpg\";s:11:\"client_name\";s:35:\"63896_108078572597269_5894492_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:43.659999999999997;s:8:\"is_image\";b:1;s:11:\"image_width\";i:333;s:12:\"image_height\";i:500;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"333\" height=\"500\"\";}'),(160,'','','2012-06-30 06:43:43','0','a:14:{s:9:\"file_name\";s:38:\"480930_335119676559823_876687281_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:95:\"/media/sf_Website_Folder/iphotography/public/user/images/480930_335119676559823_876687281_n.jpg\";s:8:\"raw_name\";s:34:\"480930_335119676559823_876687281_n\";s:9:\"orig_name\";s:38:\"480930_335119676559823_876687281_n.jpg\";s:11:\"client_name\";s:38:\"480930_335119676559823_876687281_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:62.009999999999998;s:8:\"is_image\";b:1;s:11:\"image_width\";i:720;s:12:\"image_height\";i:480;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"720\" height=\"480\"\";}'),(161,'','','2012-06-30 06:43:52','0','a:14:{s:9:\"file_name\";s:36:\"222383_141823912556068_5769504_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:93:\"/media/sf_Website_Folder/iphotography/public/user/images/222383_141823912556068_5769504_n.jpg\";s:8:\"raw_name\";s:32:\"222383_141823912556068_5769504_n\";s:9:\"orig_name\";s:36:\"222383_141823912556068_5769504_n.jpg\";s:11:\"client_name\";s:36:\"222383_141823912556068_5769504_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:27.559999999999999;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:399;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"399\"\";}'),(162,'','','2012-06-30 06:44:02','0','a:14:{s:9:\"file_name\";s:36:\"224895_141455309259595_7428758_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:93:\"/media/sf_Website_Folder/iphotography/public/user/images/224895_141455309259595_7428758_n.jpg\";s:8:\"raw_name\";s:32:\"224895_141455309259595_7428758_n\";s:9:\"orig_name\";s:36:\"224895_141455309259595_7428758_n.jpg\";s:11:\"client_name\";s:36:\"224895_141455309259595_7428758_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:49.850000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:600;s:12:\"image_height\";i:468;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"600\" height=\"468\"\";}'),(163,'','','2012-06-30 06:44:11','0','a:14:{s:9:\"file_name\";s:36:\"247882_149629161775543_1951925_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:93:\"/media/sf_Website_Folder/iphotography/public/user/images/247882_149629161775543_1951925_n.jpg\";s:8:\"raw_name\";s:32:\"247882_149629161775543_1951925_n\";s:9:\"orig_name\";s:36:\"247882_149629161775543_1951925_n.jpg\";s:11:\"client_name\";s:36:\"247882_149629161775543_1951925_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:58.390000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:473;s:12:\"image_height\";i:600;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"473\" height=\"600\"\";}'),(164,'','','2012-06-30 07:34:09','0','a:14:{s:9:\"file_name\";s:36:\"254647_169073356497790_5028151_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:93:\"/media/sf_Website_Folder/iphotography/public/user/images/254647_169073356497790_5028151_n.jpg\";s:8:\"raw_name\";s:32:\"254647_169073356497790_5028151_n\";s:9:\"orig_name\";s:36:\"254647_169073356497790_5028151_n.jpg\";s:11:\"client_name\";s:36:\"254647_169073356497790_5028151_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:97.459999999999994;s:8:\"is_image\";b:1;s:11:\"image_width\";i:700;s:12:\"image_height\";i:467;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"700\" height=\"467\"\";}'),(165,'','','2012-06-30 07:34:30','0','a:14:{s:9:\"file_name\";s:35:\"282492_170462266358899_789568_n.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:92:\"/media/sf_Website_Folder/iphotography/public/user/images/282492_170462266358899_789568_n.jpg\";s:8:\"raw_name\";s:31:\"282492_170462266358899_789568_n\";s:9:\"orig_name\";s:35:\"282492_170462266358899_789568_n.jpg\";s:11:\"client_name\";s:35:\"282492_170462266358899_789568_n.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:120.18000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:467;s:12:\"image_height\";i:700;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"467\" height=\"700\"\";}'),(166,'','','2012-06-30 07:37:42','0','a:14:{s:9:\"file_name\";s:35:\"286524_172275272844265_324610_o.jpg\";s:9:\"file_type\";s:10:\"image/jpeg\";s:9:\"file_path\";s:57:\"/media/sf_Website_Folder/iphotography/public/user/images/\";s:9:\"full_path\";s:92:\"/media/sf_Website_Folder/iphotography/public/user/images/286524_172275272844265_324610_o.jpg\";s:8:\"raw_name\";s:31:\"286524_172275272844265_324610_o\";s:9:\"orig_name\";s:35:\"286524_172275272844265_324610_o.jpg\";s:11:\"client_name\";s:35:\"286524_172275272844265_324610_o.jpg\";s:8:\"file_ext\";s:4:\".jpg\";s:9:\"file_size\";d:80.390000000000001;s:8:\"is_image\";b:1;s:11:\"image_width\";i:750;s:12:\"image_height\";i:300;s:10:\"image_type\";s:4:\"jpeg\";s:14:\"image_size_str\";s:24:\"width=\"750\" height=\"300\"\";}');
/*!40000 ALTER TABLE images ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS albums;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE albums (
  id int(11) NOT NULL AUTO_INCREMENT,
  label varchar(100) NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES albums WRITE;
/*!40000 ALTER TABLE albums DISABLE KEYS */;
/*!40000 ALTER TABLE albums ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS categories;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  description varchar(250) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES categories WRITE;
/*!40000 ALTER TABLE categories DISABLE KEYS */;
INSERT INTO categories (id, name, description, type) VALUES (1,'Freebies','Free UI, PSD, CSS, HTML and Snippet','post');
/*!40000 ALTER TABLE categories ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS post_meta;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE post_meta (
  id int(11) NOT NULL AUTO_INCREMENT,
  post_id int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  content text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES post_meta WRITE;
/*!40000 ALTER TABLE post_meta DISABLE KEYS */;
INSERT INTO post_meta (id, post_id, type, content) VALUES (27,0,'post_link','meong'),(28,33,'post_link',' meong2'),(29,33,'poster','poster2'),(31,0,'date_happen','2012:6:7 6:11:00'),(33,33,'date_happen','2012:7:10 13:0:00'),(34,0,'post_link','link'),(35,0,'poster','poseter'),(36,35,'post_link','http://kaskus.com'),(40,36,'primary_image','54'),(41,33,'primary_image','160'),(42,35,'primary_image','84'),(44,37,'primary_image','152'),(45,0,'primary_image','112'),(46,0,'primary_image','129'),(51,38,'meong',''),(52,38,'publish','checked'),(55,38,'currentproject','checked'),(56,38,'primary_image','83'),(57,35,'currentproject',''),(58,35,'publish',''),(59,40,'currentproject',''),(60,40,'publish','checked'),(61,33,'currentproject',''),(62,33,'publish','checked'),(63,0,'currentproject',''),(64,0,'publish',''),(65,40,'primary_image','139'),(66,44,'currentproject',''),(67,44,'publish','checked'),(68,44,'primary_image','146'),(69,37,'currentproject',''),(70,37,'publish','checked'),(71,0,'currentproject',''),(72,0,'publish',''),(73,46,'currentproject',''),(74,46,'publish','checked'),(75,0,'currentproject',''),(76,0,'publish','');
/*!40000 ALTER TABLE post_meta ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS groups;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE groups (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  description varchar(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES groups WRITE;
/*!40000 ALTER TABLE groups DISABLE KEYS */;
INSERT INTO groups (id, name, description) VALUES (1,'admin','Administrator'),(2,'members','General User');
/*!40000 ALTER TABLE groups ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS hire;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE hire (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  email varchar(200) DEFAULT NULL,
  job_description text,
  other_contact text,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES hire WRITE;
/*!40000 ALTER TABLE hire DISABLE KEYS */;
INSERT INTO hire (id, name, email, job_description, other_contact, datetime) VALUES (6,'name','email@email.com','descritpion web webwbwebe','another conatacc',NULL),(7,'name','email@email.com','descritpion web webwbwebe','another conatacc',NULL),(8,'name','email@email.com','descritpion web webwbwebe','another conatacc','2012-06-15 18:37:10'),(9,'asdf','asdf','asdf','asdf','2012-06-20 01:28:44'),(10,'asdf','asdf','asdf','asdf','2012-06-28 04:16:46'),(11,'456','456','456','456','2012-06-28 04:17:20');
/*!40000 ALTER TABLE hire ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS products_bridge;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE products_bridge (
  id int(11) NOT NULL,
  product_id int(11) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES products_bridge WRITE;
/*!40000 ALTER TABLE products_bridge DISABLE KEYS */;
/*!40000 ALTER TABLE products_bridge ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS posts;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE posts (
  id int(11) NOT NULL AUTO_INCREMENT,
  post_date datetime DEFAULT NULL,
  post_title tinytext NOT NULL,
  post_content text NOT NULL,
  post_modified datetime DEFAULT NULL,
  post_type varchar(100) NOT NULL,
  location varchar(100) DEFAULT NULL,
  parentpost_id int(11) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES posts WRITE;
/*!40000 ALTER TABLE posts DISABLE KEYS */;
INSERT INTO posts (id, post_date, post_title, post_content, post_modified, post_type, location, parentpost_id, category_id) VALUES (33,'2012-06-22 01:24:01','Prewedding','<p></p><h2>PREWEDDING PACKAGE LIST<br></h2><h2> Regular Package</h2>  Rp. 2.500.000<br> 2 Outdoor photo location ( 1 day photo shoot)<br> 2 printed photos with classic frame 40x60cm<br> 2 printed photos with classic frame 50x75cm<br> DVD all File<br> <br><h2> Exclusive Package </h2> Rp. 5.000.000<br> 4 Outdoor photo location ( 2 day photo shoot)<br> 2 printed photos with classic frame 40x60cm<br> 4 printed photos with classic frame 50x75cm<br> Free Makeup &amp; Hair  do<br> 1 Exclusive wedding book 8 RS -10 sheet<br> DVD all File<br> <br><h2> Luxury Package</h2>  Rp. 7.500.000<br> 5 Outdoor photo location ( 2 full day photo shoot)<br> 3 printed photos with classic frame 40x60cm<br> 5 printed photos with classic frame 50x75cm<br> Free Makeup &amp; Hair  do<br> 1 Exclusive wedding book 10 R -10 sheet<br> 10 photos full editing soft copy<br> DVD all File<p></p>','2012-05-25 17:20:48','album','',NULL,NULL),(35,'2012-06-28 08:53:35','wwee','<p>Wuhu</p>','2012-05-25 17:21:25','draft',NULL,NULL,NULL),(36,'2012-05-30 12:51:36','Blog with Quote','<p>Like you, I used to think the world was this great place where everybody lived by the same standards I did, then some kid with a nail showed me I was living in his world, a world where chaos rules not order, a world where righteousness is not rewarded. That\'s Cesar\'s world, and if you\'re not willing to play by his rules, then you\'re gonna have to pay the price.</p>\n\n<quote>The lysine contingency - it\'s intended to prevent the spread of the animals is case they ever got off the island. Dr. Wu inserted a gene that makes a single faulty enzyme in protein metabolism. The animals can\'t manufacture the amino acid lysine. Unless they\'re continually supplied with lysine by us, they\'ll slip into a coma and die.</quote>',NULL,'blog',NULL,NULL,NULL),(37,'2012-06-28 08:34:05','Dreamy Photography','<a href=\"http://dreamy.kodingen.com\">dreamy.kodingen.com</a><br><br>Situs Portfolio photography<br><br>halaman : <br>Home<br>Album - Album Details<br>Contact - Us<br><br>Dibuat Menggunakan<br>PHP-Codeigniter, Datamapper, Twig Template,<br>Less.js, colorbox.js, revolver.js<br>',NULL,'portfolio',NULL,NULL,NULL),(38,'2012-06-21 04:32:11','Portfolioway','<p>Proin nonummy, lacus eget pulvinar lacinia, pede felis dignissim leo, vitae tristique magna lacus sit amet eros. Nullam ornare. Praesent odio ligula, dapibus sed, tincidunt eget, dictum ac, nibh. Nam quis lacus. Nunc eleifend molestie velit. Morbi lobortis quam eu velit. Donec euismod vestibulum massa. Donec non lectus. Aliquam commodo lacus sit amet nulla. Cras dignissim elit et augue. Nullam non diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Aenean vestibulum. Sed lobortis elit quis lectus. Nunc sed lacus at augue bibendum dapibus.</p>','2012-05-25 20:07:43','portfolio',NULL,NULL,NULL),(40,'2012-06-22 02:01:19','Event','<p><br></p>',NULL,'album',NULL,NULL,NULL),(42,'2012-05-25 18:29:43','Security Helper','<h2>The Security Helper file contains security related functions.</h2><h2>xss_clean()</h2>\n\n<p>Provides Cross Site Script Hack filtering.  This function is an alias to the one in the\n<a href=\"http://seturan.dev/user_guide/libraries/input.html\">Input class</a>.  More info can be found there.</p>\n\n\n<h2>sanitize_filename()</h2>\n\n<p>Provides protection against directory traversal.  This function is an alias to the one in the\n<a href=\"http://seturan.dev/user_guide/libraries/security.html\">Security class</a>.  More info can be found there.</p>\n\n\n<h2>do_hash()</h2>\n\n<p>Permits you to create SHA1 or MD5 one way hashes suitable for encrypting passwords.  Will create SHA1 by default. Examples:</p>\n\n<code>\n$str = do_hash($str); // SHA1<br>\n$str = do_hash($str, \'md5\'); // MD5\n</code>\n\n<p class=\"important\"><strong>Note:</strong>  This function was formerly named <kbd>dohash()</kbd>, which has been deprecated in favour of <kbd>do_hash()</kbd>.</p>\n\n\n\n<h2>strip_image_tags()</h2>\n\n<p>This is a security function that will strip image tags from a string.  It leaves the image URL as plain text.</p>\n\n<code>$string = strip_image_tags($string);</code>\n\n\n<h2>encode_php_tags()</h2>\n\n<p>This is a security function that converts PHP tags to entities. Note:\n If you use the XSS filtering function it does this automatically.</p>\n\n<code>$string = encode_php_tags($string);</code>','2012-05-25 20:36:40','blog',NULL,NULL,NULL),(44,'2012-06-22 03:56:53','Close Up, Portrait, Make Up','',NULL,'album',NULL,NULL,NULL),(46,'2012-06-30 07:32:32','Outdoor','<p><br></p>',NULL,'subalbum',NULL,33,NULL);
/*!40000 ALTER TABLE posts ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS products;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE products (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  price int(11) DEFAULT NULL,
  description text,
  tag varchar(200) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  views int(11) DEFAULT NULL,
  publish int(11) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  image_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES products WRITE;
/*!40000 ALTER TABLE products DISABLE KEYS */;
INSERT INTO products (id, name, price, description, tag, datetime, views, publish, category_id, image_id) VALUES (1,'123',123,'<p>Initial content 2 444<br></p>','123','2012-06-13 21:27:52',NULL,0,NULL,135),(2,'123456',123456,'<p>Initial content</p>','tag tag tag','2012-06-12 22:18:33',NULL,0,NULL,NULL),(3,'namaproduck',2340,'<p>Contemporary Muslims who bring up the memory of Cordoba typically do so \neither to emphasize the need for a new Islamic scientific and industrial\n renaissance, or to emphasize the need for a multi-cultural and tolerant\n society.</p>','tag tag','2012-06-08 17:24:17',NULL,0,NULL,NULL),(4,'123',123,'<p>Initial content</p>','tag tag tag','2012-06-08 17:25:32',NULL,0,NULL,NULL);
/*!40000 ALTER TABLE products ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS images_bridge;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE images_bridge (
  id int(11) NOT NULL AUTO_INCREMENT,
  image_id int(11) DEFAULT NULL,
  post_id int(11) DEFAULT NULL,
  product_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES images_bridge WRITE;
/*!40000 ALTER TABLE images_bridge DISABLE KEYS */;
INSERT INTO images_bridge (id, image_id, post_id, product_id) VALUES (126,110,NULL,3),(129,78,38,NULL),(130,83,38,NULL),(131,82,38,NULL),(132,78,35,NULL),(133,84,35,NULL),(137,139,40,NULL),(138,140,40,NULL),(139,141,40,NULL),(140,142,40,NULL),(141,143,40,NULL),(142,144,40,NULL),(144,146,44,NULL),(145,147,44,NULL),(148,150,44,NULL),(149,151,44,NULL),(150,152,37,NULL),(162,157,33,NULL),(163,158,33,NULL),(164,159,33,NULL),(165,160,33,NULL),(166,161,33,NULL),(167,162,33,NULL),(169,164,46,NULL);
/*!40000 ALTER TABLE images_bridge ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS post_categories;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE post_categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  description varchar(250) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES post_categories WRITE;
/*!40000 ALTER TABLE post_categories DISABLE KEYS */;
/*!40000 ALTER TABLE post_categories ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

