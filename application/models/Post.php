<?php

 /*
  * To change this template, choose Tools | Templates
  * and open the template in the editor.
  */

 /**
  * Description of Post
  *
  * @author olivopablo
  */
 class Post extends DataMapper
 {

     var $has_one =
             array(
         'parentpost' => array('class' => 'Post', 'other_field' => 'childpost'),
         'category' => array('class' => 'category', 'other_field' => 'category')
     );
     var $has_many =
             array(
         'image' => array('join_table' => 'images_bridge', 'auto_populate' => TRUE),
         'post_meta' => array('class' => 'post_meta'),
         'childpost' => array('class' => 'Post', 'other_field' => 'parentpost')
     );

     function __construct($id = NULL)
     {
         parent::__construct($id);
     }

     function get_titles()
     {
         return $this->select('post_title')->get()->all_to_array();
     }

     function get_meta()
     {

         if(!isset($image_mdl))
             $image_mdl = new Image();

         $raw_meta = $this->where('id', $this->id)->include_related('post_meta')->get();


         //convert to [post_meta] = array('type'=>'content')
         foreach($raw_meta->all as $meta)
         {
             $post_meta[$meta->post_meta_type] = $meta->post_meta_content;
             //if primary images exists get and unserialize
             if($meta->post_meta_type == 'primary_image')
             {
                 //get images from images id
                 $images = $image_mdl->where('id', $meta->post_meta_content)->get();
                 //if image exists on images table
                 if($images->meta)
                 {
                     //unserialize meta fields from images table
                     $post_meta[$meta->post_meta_type] = unserialize($images->meta);
                 }
                 else
                 {
                     $post_meta[$meta->post_meta_type] = array('file_name' => 'none.jpg');
                 }
             }
         }
         return $post_meta;
     }

     /**
      * get by type
      * @param type $type
      * @param type $limit
      * @return type 
      */
     function get_lists_by_type($post_type, $limit = 1)
     {
         $last_id = end($this->all);
         $mdl['previous'] = $this->where('post_type', $post_type)->where('id <', $last_id->id, $limit)->order_by('id', 'DESC')->get()->all_to_array();
         $mdl['next'] = $this->where('post_type', $post_type)->where('id >', $last_id->id, $limit)->order_by('id', 'ASC')->get()->all_to_array();
         $mdl['all'] = $this->where('post_type', $post_type)->order_by('id', 'DESC')->get('10')->all_to_array();
         ;

         $mdl['previous'] = current($mdl['previous']);
         $mdl['next'] = current($mdl['next']);
         return $mdl;
     }

     /**
      * set post_type i.e this->posts('portfolio')
      * @param type $type
      * @param type $post_id
      * @return type 
      */
     function posts($type = null, $post_id = null)
     {
         $posts_mdl = $this->where('post_type', $type);
         if($post_id)
         {
             $posts_mdl->where('id', $post_id);
         }
         return $posts_mdl;
     }

     /**
      *
      * @return type array(images)
      */
     function get_images($limit = null, $offset = null)
     {
         if(!isset($image_mdl))
             $image_mdl = new Image();
         //get image(s)
         $iterated = $image_mdl->include_related('posts')->where('post_id', $this->id)->order_by('id', 'DESC')->get($limit, $offset)->all_to_array();


         //looping alternative instead foreach
         $key = array_keys($iterated);
         $size = sizeOf($key);
         for($i = 0; $i < $size; $i++)
         {
             $iterated[$key[$i]]['data'] = unserialize($iterated[$key[$i]]['meta']);
         }


         return $iterated;
     }

     /**
      * get images attach as array
      */
     function get_images_list($limit = 4)
     {

         $key = array_keys($this->all);
         $size = sizeOf($key);
         for($i = 0; $i < $size; $i++)
         {
             $this->all[$key[$i]]->images = $this->all[$key[$i]]->get_images($limit);
         }

         return $this;
     }

     function get_post_meta()
     {

         $key = array_keys($this->all);
         $size = sizeOf($key);
         for($i = 0; $i < $size; $i++)
         {
             $this->all[$key[$i]]->post_meta = $this->all[$key[$i]]->get_meta();
         }
         return $this;
     }

 }