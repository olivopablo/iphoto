<?php

class Schedule_mdl extends DataMapper {

    var $table = 'schedule';

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    function sort_date() {

        if (isset($_GET['date_month'])) {
            $this->where("month(date_happen) = " . $_GET['date_month']);
        } else {
            $this->where('date_happen >=', timestamp_to_mysqldatetime());
        }

        return $this;
    }

    function save_schedule() {

        $ci = & get_instance();
        $post = $ci->input->post();
        $id = $post['id'] > 0 ? $this->id = $post['id'] : null;
        //parse set to $this->field = $this->input->post('save_field');
        $save_field = array('name', 'description');
        foreach ($save_field as $set) {
            //check if $_POST not null else set on this->
            if ($ci->input->post($set)) {
                $this->{$set} = $ci->input->post($set);
            }
        }


        //set datetime. if id isset then dont 

        $this->datetime = timestamp_to_mysqldatetime();


        $date_due = $ci->input->post('date_due');
        //date happen checked
        if ($date_due['confirm'] == 1) {
            $mysqldate = $date_due['year'] . ":" . $date_due['month'] . ":" . $date_due['day'] . " " . $date_due['hour'] . ":" . $date_due['minute'] . ":00";

            $this->date_due = $mysqldate;
        }


        $date_happen = $ci->input->post('date_happen');
        //date happen checked
        if ($date_happen['confirm'] == 1) {
            $mysqldate = $date_happen['year'] . ":" . $date_happen['month'] . ":" . $date_happen['day'] . " " . $date_happen['hour'] . ":" . $date_happen['minute'] . ":00";
            $this->date_happen = $mysqldate;
        }



        if ($this->save()) {

            return true;
        }


    }

}
