<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author olivopablo
 */
class Image extends DataMapper {

    var $table = 'images';
    var $has_many = array('post' => array('join_table' => 'images_bridge'));

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    function save_to($classname, $id) {

        $mdl = new $classname;
        //get post
        $mdl->get_by_id($id);
        //get images

        if ($mdl) {
            //save relationship
            if ($this->save($mdl)) {
                return true;
            }
        }
    }

    function delete_from($model, $image_id, $id) {
        $mdl = new $model;
        $mdl->get_by_id($id);

        $this->get_by_id($image_id);

        //delete realtionship .. in images_bridge
        if ($this->delete($mdl)) {

            if ($model == 'Post') {
                $meta_mdl = new Post_meta();
                $meta_mdl->where('post_id', $post_id)->where('content', $image_id)->get(1);
                //delete primary images if any
                $meta_mdl->delete();
            }
            return true;
        }
    }

    function update_pic($post) {
        $this->where('id', $post['image_id'])->get();
        if ($this != null && ($post['label'] != null || $post['description'] != null)) {
            $this->label = $post['label'];
            $this->description = $post['description'];
            if ($this->save()) {
                return true;
            }
        }
    }

    function get_posts() {
        $images = $this->all_to_array();
        $post = new Post();

        //loop for many image
        foreach ($images as $key => $value) {
            $array = null;
            $array = $post->select(array('post_title', 'post_type', 'id'))->where_related_image('id', $images[$key]['id'])->get()->all_to_array();

            if ($array) {
                $images[$key]['posts'] = $array;
            }

            //unserialize images meta
            $images[$key]['data'] = unserialize($images[$key]['meta']);
        }


        return $images;
    }

    function test_query() {


        $post = new Post();
        for ($i = 0; $i < 10000; $i++) {
            $post->get();
        }
    }

    function get_all() {

        //get image(s)
        $iterated = $this->get()->all_to_array();

        foreach ($iterated as $key => $value) {
            //unserialize each image meta field
            $iterated[$key]['data'] = unserialize($iterated[$key]['meta']);
        }

        return $iterated;
    }

    function get_one() {
        //get image(s)
        $iterated = $this->to_array();
        $iterated['data'] = unserialize($iterated['meta']);
        return $iterated;
    }

    function save_to_images() {

        $ci = & get_instance();

        $this->label = $ci->input->post('label');
        $this->description = $ci->input->post('description');
        $this->date = timestamp_to_mysqldatetime();
        //$images_mdl->date_captured= $this->input->post('date');
        $this->publish = $ci->input->post('publish');
        $this->meta = serialize($ci->upload->data());
        if ($this->save()) {
            //fire::debug_message($images_mdl);
            //if post_id exists
            $post_id = $ci->input->post('post_id');
            if ($post_id) {
                $this->save_to('Post', $post_id);
            }

            $product_id = $ci->input->post('product_id');
            if ($product_id) {
                $this->save_to('Product', $product_id);
            }

            return true;
        }
    }

    /**
     * delete permanent from database and disk
     * @param type $image_id
     * @return type 
     */
    function delete_permanent($image_id) {

        $this->get_by_id($image_id);

        //get images data
        $delete_file = unserialize($this->meta);
        if ($this->delete()) {
            fire::debug_message($delete_file);
            //delete file from server
            unlink($this->config->item('images_url') . $delete_file['file_name']);
            unlink($this->config->item('images_url') . $delete_file['raw_name'] . "_thumb" . $delete_file['file_ext']);
            return true;
        }
    }

    function get_recent($limit=null){
        $prepare = $this->order_by('id','DESC')->limit($limit);
        return $prepare->get_all();
    }

}

?>
