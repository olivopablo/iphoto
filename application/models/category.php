<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author olivopablo
 */
class Category extends DataMapper
{

    var $has_many = array('post');

    function __construct($id = NULL)
    {
        parent::__construct($id);
    }

    function save_category()
    {
        $ci                = & get_instance();
        $post              = $ci->input->post();
        $this->id          = empty($post['id']) ? null : $post['id'];
        $this->name        = $post['name'];
        $this->description = $post['description'];
        $this->type        = $post['type'];
        $this->save();
    }
    
    
     function get_category($type='', $limit='')
     {
         $model = new Category();
         if($type !== '')
         {
             $model->where('type', $type);
         }
         return $model->get($limit)->all;
     }

}

?>
