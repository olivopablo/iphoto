<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author olivopablo
 */
class User extends DataMapper {

	var $has_many = array('post_meta','image');


	function __construct($id = NULL) {
		parent::__construct($id);
	}
	
	function auth($username=null,$password=null){
		
		if($username == $password){
		return array('meong','meong');
		}
	}

	/**
	 * select post empty parameter to select all
	 * @param type $post_id or string_query where
	 * @param type $field table
	 * @return type array()
	 */
	function selectPost($field='id', $post_id=null) {

		if ($post_id) {
			$posts = $this->where_in($field, $post_id)->get();
		} else {
			$posts = $this->get();
		}

		$iterated = $posts->all_to_array();
		$Post_meta = new Post_meta();
		foreach ($iterated as $key => $value) {
			$iterated[$key]['post_meta'] = $Post_meta->where('post_id', $iterated[$key]['id'])->get()->all_to_array();
		}


		return $iterated;
	}

	/**
	 * select post empty parameter to select all
	 * @param type $post_id or string_query where
	 * @param type $field table
	 * @return type array()
	 */
	function select_post($field='id', $post_id=null) {

		if ($post_id) {
			$posts = $this->where_in($field, $post_id)->get();
		} else {
			$posts = $this->get();
		}

		$iterated = $posts->all_to_array();
		$Post_meta = new Post_meta();
		foreach ($iterated as $key => $value) {
			$raw_meta = $Post_meta->where('post_id', $iterated[$key]['id'])->get()->all_to_array();


			//convert to [post_meta] = array('type'=>'content')
			foreach ($raw_meta as $meta) {
				$iterated[$key]['post_meta'][$meta['type']] = $meta['content'];
			}
		}


		return $iterated;
	}

	/**
	 * get meta from query. it'll convert to array and attach meta to [post_meta]
	 * return array
	 * @param type $post_id or string_query where
	 * @param type $field table
	 * @return type array()
	 */
	function get_meta() {

	
		$iterated = $this->all_to_array();//$this is object from previous chaining
		$Post_meta = new Post_meta();
		foreach ($iterated as $key => $value) {
			$raw_meta = $Post_meta->where('post_id', $iterated[$key]['id'])->get()->all_to_array();


			//convert to [post_meta] = array('type'=>'content')
			foreach ($raw_meta as $meta) {
				$iterated[$key]['post_meta'][$meta['type']] = $meta['content'];
			}
		}


		return $iterated;
	}

}

?>
