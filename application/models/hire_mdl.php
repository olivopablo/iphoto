<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post
 *
 * @author olivopablo
 */
class Hire_mdl extends DataMapper {

    var $table = 'hire';

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    function save_hire() {

        $ci = & get_instance();

        //parse set to $this->field = $this->input->post('save_field');
        $save_field = array('name', 'email', 'other_contact', 'job_description', 'datetime');
        foreach ($save_field as $set) {

            //check if $_POST not null else set on this->
            if ($ci->input->post($set)) {
                $this->{$set} = $ci->input->post($set);
            }
            
            //set datetime
            if ($set == 'datetime') {
                $this->datetime = timestamp_to_mysqldatetime();
            }
        }


        if ($this->save()) {
            //fire::debug_message($images_mdl);
            return true;
        }
    }
    


    /**
     * delete permanent from database and disk
     * @param type $image_id
     * @return type 
     */
}

?>
