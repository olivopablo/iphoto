<?php

/**
 * Convert timestamp to Human Date
 *
 * Returns the date (format according to given string) of a given timestamp
 *
 * @author    Cleiton Francisco V. Gomes <http://www.cleiton.net/>
 * @access    public
 * @param     string
 * @param     string
 * @return    string
 */
function year_options($year=null) {

	$thn_lahir_option = null;
	for ($t = 2012; $t <= date("Y") + 10; $t++) {

		if ($t == $year) {
			$thn_lahir_option .= "<option value=\"$t\"
selected>$t</option>";
		} else {
			$thn_lahir_option .= "<option
value=\"$t\">$t</option>";
		}
	}

	return $thn_lahir_option;
}

function hour_options($hour=null) {

	$hour_options = null;
	for ($t = 00; $t <= 24; $t++) {
		if ($t == $hour) {
			$hour_options .= "<option value=\"$t\"
selected>$t</option>";
		} else {
			$hour_options .= "<option
value=\"$t\">$t</option>";
		}
	}

	return $hour_options;
}

function minute_options($minute=null) {

	$hour_options = null;
	for ($t = 00; $t <= 60; $t++) {
		if ($t == $minute) {
			$hour_options .= "<option value=\"$t\"
selected>$t</option>";
		} else {
			$hour_options .= "<option
value=\"$t\">$t</option>";
		}
	}

	return $hour_options;
}

function date_options($date=null) {

	$gmt = date("Z");

	$tgl_lahir_option = null;
	for ($t = 1; $t <= 31; $t++) {

		if ($t == $date) {
			$tgl_lahir_option .= "<option value=\"$t\"
selected>$t</option>";
		} else {
			$tgl_lahir_option .= "<option
value=\"$t\">$t</option>";
		}
	}

	return $tgl_lahir_option;
}

function month_options($bulan=null) {

	$namaBulan = array("null", "Januari", "Februari", "Maret",
	    "April", "Mei", "Juni",
	    "Juli", "Agustus",
	    "September", "Oktober", "November", "Desember");

	$bln_lahir_option = null;
	for ($t = 1; $t <= 12; $t++) {

		if ($t == $bulan) {
			$bln_lahir_option .= "<option value=\"$t\"
selected>$namaBulan[$t]</option>";
		} else {
			$bln_lahir_option .= "<option
value=\"$t\">$namaBulan[$t]</option>";
		}
	}

	return $bln_lahir_option;
}

/**
 * get post.post_meta value
 * @param	array 
 * @param	string 
 * @return string of post_meta.content
 */
function get_meta_value($array_post_meta = null, $type=null) {

	if ($type && $array_post_meta ) {
		foreach ($array_post_meta as $meta) {
			if ($meta['type'] == $type) {
				return $meta['content'];
			}
		}
	}
}


?>
