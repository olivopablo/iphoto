<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author olivopablo
 */
class index extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $post_mdl = new Post();
        $img_mdl = new Image();
        
        $post_mdl->posts('album')->get();
        $view_data['posts'] = $post_mdl->get_images_list()->all;
        $view_data['carousels'] = $img_mdl->get_recent(5);

        $view_data['title'] = "i photography";
        $view_data['welcome'] = "Welcome";

        $this->twig->display('index.html', $view_data);
    }

}

?>
