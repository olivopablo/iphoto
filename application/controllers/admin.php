<?php

 /*
  * To change this template, choose Tools | Templates
  * and open the template in the editor.
  */

 /**
  * Description of index
  *
  * @author olivopablo
  */
 class admin extends CI_Controller
 {

     private $img_dir;
     public $uploaded_file;

     function __construct()
     {
         parent::__construct();

         $this->load->library('Ion_auth');

         if (!$this->ion_auth->logged_in())
         {
             redirect(base_url() . 'auth/login/', 'location');
         }
     }

     function php()
     {
         $view_data['posts'] = phpinfo();
         $this->twig->display('admin/phpinfo.html', $view_data);
     }

     function index()
     {
         $post_mdl = new Post();
         $view_data['posts'] = $post_mdl->where_not_in('post_type', array('subalbum'))->get()->get_post_meta()->all;
         $this->twig->display('admin/index.html', $view_data);
     }

     function albums()
     {
         $post_mdl = new Post();
         $post_type = array('album');
         $view_data['posts'] = $post_mdl->where_in('post_type', $post_type)->get()->get_post_meta()->all;
         $this->twig->display('admin/index.html', $view_data);
     }

     function logout()
     {

         if ($this->ion_auth->logout())
         {
             redirect(base_url() . 'auth/login', 'refresh');
         }
     }

     function hire($action = null, $id = null)
     {
         $model = new Hire_mdl();

         switch ($action)
         {
             case 'delete':
                 //get all meta use where in	
                 $model->where('id', $id)->get();
                 //delete post and meta
                 $model->delete();
                 break;
         }

         $view_data['hires'] = $model->get()->all;
         $this->twig->display('admin/Hire.html', $view_data);
     }

     function schedule()
     {
         $model = new Schedule_mdl();

         $view_data['schedules'] = $model->sort_date()->order_by('date_happen', 'ASC')->get()->all;

         //set current date for  form_schedule
         $view_data['schedule']['date_due'] = $view_data['schedule']['date_happen'] = mysqldatetime_to_array();

         $this->twig->display('admin/schedule.html', $view_data);
     }

     function gallery()
     {
         $image_mdl = new Image();
         $view_data['images'] = $image_mdl->get()->get_posts();
         $this->twig->display('admin/gallery.html', $view_data);
     }

     function product($crud = null, $id = null)
     {
         $mdl = new Product();
         switch ($crud)
         {
             case 'update':
                 $view_data['product'] = $mdl->get_by_id($id)->get_primary_image()->all[0];
                 $view_data['images'] = $mdl->get_images();
                 $this->twig->display('admin/product_update.html', $view_data);
                 break;

             case 'delete':
                 $view_data['products'] = $mdl->get_by_id($id);
                 $this->twig->display('admin/product_update.html', $view_data);
                 break;

             case 'ko':
                 $this->twig->display('ko/product.html');
                 break;


             default:
                 $view_data['products'] = $mdl->get()->get_primary_image();
                 $this->twig->display('admin/product.html', $view_data);
                 break;
         }
     }

     function images()
     {
         $arg = $this->input->get();
         switch ($arg['action'])
         {
             case "set_primary" :
                 $post_meta_mdl = new Post_meta();
                 $post_meta_mdl->where('type', 'primary_image')->where('post_id', $arg['post_id'])->get();
                 $post_meta_mdl->post_id = $arg['post_id'];
                 $post_meta_mdl->type = 'primary_image';
                 $post_meta_mdl->content = $arg['image_id'];

                 if ($post_meta_mdl->save())
                 {
                     redirect(urldecode($arg['source_url']), 'location');
                 }
                 else
                 {
                     echo "error";
                 }
                 break;

             case "delete_from_post" :
                 $image_mdl = new Image();
                 $image_mdl->delete_from('Post', $arg['image_id'], $arg['post_id']);
                 if ($image_mdl)
                 {
                     redirect(urldecode($arg['source_url']), 'location');
                 }
                 else
                 {
                     echo "error";
                 }
                 break;

             case "delete_from_product" :
                 $image_mdl = new Image();
                 $image_mdl->delete_from('Product', $arg['image_id'], $arg['product_id']);
                 if ($image_mdl)
                 {
                     redirect(urldecode($arg['source_url']), 'location');
                 }
                 else
                 {
                     echo "error";
                 }
                 break;

             case "delete_permanent" :
                 $images_mdl = new Image();
                 $images_mdl->delete_permanent($arg['image_id']);
                 if ($images_mdl)
                 {
                     redirect(urldecode($arg['source_url']), 'location');
                 }
                 else
                 {
                     echo "error";
                 }
                 break;

             default :
                 echo "404";
                 exit;
         }
     }

     function childpost($action, $id=null, $description=null)
     {

         $post_mdl = new Post($id);
         switch ($action)
         {
             case 'update':
                 $view_data['posts'] = $post_mdl->get_images_list(100)->get_post_meta()->all;
                 $view_data['parentpost'] = $post_mdl->parentpost->get()->get_post_meta()->all;
                 break;

             case 'add' :
                 $view_data['parentpost'] = $post_mdl->all;
                 break;

             case 'delete' :
                  if ($post_mdl->delete())
                 {
                     redirect(urldecode($_GET['source_url']), 'location');
                 }
                 else
                 {
                     $view_data = array();
                 }
                 break;
         }
         $this->twig->display('admin/post_update.html', $view_data);
     }

     function post($action, $id=null, $description=null)
     {

         $post_mdl = new Post($id);
         switch ($action)
         {
             case 'update':
                 $view_data['posts'] = $post_mdl->get_images_list(100)->get_post_meta()->all;
                 $view_data['childpost'] = $post_mdl->childpost->get()->get_post_meta()->all;
                 break;

             case 'add' :
                 $view_data = array();
                 break;

             case 'delete' :
                 if ($post_mdl->delete())
                 {
                     redirect(urldecode($_GET['source_url']), 'location');
                 }
                 else
                 {
                     $view_data = array();
                 }
                 break;
         }
         $this->twig->display('admin/post_update.html', $view_data);
     }

     function test()
     {
         $post_mdl = new Post();
         $output = $post_mdl->Post_meta->get();
         echo $output;
     }

     function common($object, $action=null, $id=null)
     {
         $model = new $object($id);
         $view_data = array();
         switch ($action)
         {
             case "update" :
                 $view_data['category'] = $model;
                 break;
             
             case "delete" :
                 $model->delete();
                 //redirect to common/$object page
                 redirect(base_url().'admin/common/'.$object,'location');
                 break;
             
             default:
                 $view_data['categories'] = $model->order_by('id','DESC')->get()->all;
                 break;
         };
         


         $this->twig->display("admin/$object" . "_" . "$action.html", $view_data);
     }

     function getImg()
     {
         if ($this->input->post('url'))
         {
             $this->load->config('getimg');
         }

         $view_data = null;

         return $this->twig->display('admin/getimg.html', $view_data);
     }

 }