<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author olivopablo
 */
class auth extends CI_Controller {

	function __construct() {
		parent::__construct();
	}
	
	function index(){
		return $this->login();
	}


	function login() {

		if ($this->input->post('login_token')) {
			$user = new User();
			
			//auth database
			$auth = $user->auth($this->input->post('username'),$this->input->post('password'));
			
			//if auth not null set session
			if ($auth) {
				$this->session->set_userdata('user_data',$auth);
				redirect(base_url() . "admin/", 'location');
			}else{
				
				//not corret username and password
			$view_data['flash']['error'] = "error username/password gan";	
			$this->twig->display('views/auth/login.html',$view_data);	
			}
		}
		else{
			
		//set session for login token	
		$view_data['login_token'] = md5(time());	
		$this->session->set_userdata('login_token',  $view_data['login_token'] );	
		

		$this->twig->display('views/auth/login.html',$view_data);	
		}

		
	}
	
	

	function logout() {
	$this->session->sess_destroy();
	redirect(base_url() . "auth/login/", 'location');
	}


}

?>
