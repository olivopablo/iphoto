<?php

class Contact_us extends CI_Controller {

    function __construct() {
        parent::__construct();
       
    }
    
    function index() {
        $img_mdl = new Image();
         $view_data['carousels'] = $img_mdl->get_recent(5);
        $this->twig->display('contact_us.html',$view_data);
    }

}

?>
