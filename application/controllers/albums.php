<?php

 class Albums extends CI_Controller
 {

     function __construct()
     {
         parent::__construct();
     }

     function category($id=null, $post_type=null)
     {

         $post_mdl = new Post();
         $img_mdl = new Image();

         if ($id)
         {
             //if route i.e /category/twitter/4
             $where_cond = $post_mdl->where(array('id' => $id));
         }
         elseif ($post_type)
         {
             //if route i.e /category/twitter
             $where_cond = $post_mdl->like('post_title', str_replace('-', " ", $post_type));
         }
         else
         { //if route /category/
             $where_cond = $post_mdl;
         }


         //get from database
         $posts = $where_cond->order_by('id', 'ASC')->get(1);




         //get thumbnail from images table
         $view_data['posts'] = $posts->get_images_list(100)->get_post_meta()->all;
         $view_data['childpost'] = $posts->childpost->get()->get_images_list(100)->all;

         $view_data['carousels'] = $img_mdl->get_recent(5);

         $this->twig->display('album_detail.html', $view_data);
     }

 }

?>
