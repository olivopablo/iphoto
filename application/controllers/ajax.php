<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author olivopablo
 */
class ajax extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function lazyload()
    {
        $post = $this->security->xss_clean($this->input->post());

        switch($post['load']) {
            case "form-dropdown":
                echo form_dropdown('post_type', array('portfolio' => 'porfolio', 'blog'      => 'blog', 'draft'     => 'draft', 'album'     => 'album', 'subalbum'  => 'subalbum'), $post['default']);
                break;

            case "form-dropdown-category":
                echo form_dropdown('type', array('product' => 'product', 'post'    => 'post'), $post['default']);
                break;

            case "form_upload":
                echo $this->twig->render('admin/form_upload.html');
                break;
        }
    }

    function admin($section = null, $action = null)
    {
        error_reporting(0);
        //auth for ajax Request
        $this->load->library('Ion_auth');
        if(!$this->ion_auth->is_admin())
        {
            exit;
        }

        //section for POST
        $post = $this->input->post(NULL, TRUE);

        switch($post['action']) {
            case 'post':
                $this->load->driver('drv_post');
                if ($post['post_title'] != "" || $post['post_content'] != "") {
                    $return = $this->drv_post->save_post();
                }
                echo $this->_message($return);
                break;

            case 'product':
                $product_mdl = new Product();
                if($post['name'] != "")
                {
                    $return = $product_mdl->save_product();
                }
                echo $this->_message($return);
                break;

            case 'category':
                $model = new Category();
                if($post['name'] != "")
                {
                    $return = $model->save_category();
                }
                echo $this->_message($return);
                break;

            case "save_add_from_gallery":
                $this->load->driver('drv_images');
                echo $this->drv_images->save_add_from_gallery();
                break;

            case "save_edit_pic":
                $this->load->driver('drv_images');
                echo $this->drv_images->save_edit_pic();
                break;


            case "upload_images":
                $this->load->driver('drv_images');
                $echo = $this->drv_images->upload_images();
                echo $echo;
                break;
        }


        //section for GET
        $get = $this->input->get(NULL, TRUE);

        switch($get['action']) {
            case "add_from_gallery":
                $this->load->driver('drv_images');
                echo $this->drv_images->add_from_gallery();
                break;

            case "edit_pic":
                $this->load->driver('drv_images');
                echo $this->_message($this->drv_images->edit_pic());
                break;
        }

        switch($section) {

            case "schedule":
                $model = new Schedule_mdl();
                switch($action) {
                    case "save":
                        $return = $model->save_schedule();

                        if($this->input->get('return') == 'json')
                        {
                            $this->json($model->to_json());
                        }
                        break;
                }
                break; //case schedule
        }
    }

    private function _message($return)
    {
        if($return == true)
        {
            return "<div class='alert-message success'>success</div>";
        }
        else
        {
            return "<div class='alert-message error'>error</div>";
        }
    }

    /**
     * return json or use $this->json (json_result)
     * @param type $return 
     */
    function json($return = null)
    {

        switch($_GET['action']) {
            case 'allProducts':
                $mdl    = new Product();
                $return = $mdl->get()->get_primary_image()->all_to_json();
                break;
            case 'allSchedule':
                $mdl    = new Schedule_mdl();
                $return = $mdl->sort_date()->order_by('date_happen', 'ASC')->get()->all_to_json();
                break;
        }


        $this->output
                ->set_content_type('application/json')
                ->set_output($return);
    }

    function test()
    {

        fire::debug_message($_POST);
    }

}

