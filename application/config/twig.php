<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
$config['template_dir'] = APPPATH.'views';
$config['load_functions'] = array('urlencode','strip_tags');

#$config['cache_dir'] = APPPATH.'cache/';