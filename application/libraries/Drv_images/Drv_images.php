<?php

 /*
  * To change this template, choose Tools | Templates
  * and open the template in the editor.
  */

 /**
  * Driver for produk
  */
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');

 class Drv_images extends CI_Driver_Library
 {

     var $CI;
     var $img_dir;
     var $uploaded_file;

     function __construct()
     {
         $this->CI = & get_instance();
     }

     /**
      * get add pics from images
      */
     function add_from_gallery()
     {
         $images_mdl = new Image();
         //get images gallery
         $view_data['images'] = $images_mdl->get_all();
         $view_data['id'] = $this->CI->input->get('id');
         $view_data['type'] = $this->CI->input->get('type');
         //return html
         echo $this->CI->twig->render('admin/add_from_gallery.html', $view_data);
     }

     function edit_pic()
     {
         $images_mdl = new Image();
         //get images gallery
         $view_data['images'] = $images_mdl->where('id', $this->CI->input->get('image_id'))->get()->all_to_array();
         //return html
         echo $this->CI->twig->render('admin/form_upload_edit.html', $view_data);
     }

     function save_add_from_gallery()
     {
         $post = $this->CI->input->post();
         $images_mdl = new Image();
         //get images gallery
         if ($images_mdl->where('id', $post['image_id'])->get()->save_to($post['type'], $post['id']))
         {
             echo "success";
         }
         else
         {
             echo "error";
         }
     }

     function save_edit_pic()
     {

         $images_mdl = new Image();
         //get images gallery
         if ($images_mdl->update_pic($this->CI->input->post()))
         {
             return true;
         }
     }

     function upload_images()
     {
         $this->CI->load->library('upload');
         $this->img_dir = $this->CI->config->item('images_url');

         $config['upload_path'] = $this->img_dir;
         $config['max_size'] = '2500';
         $config['allowed_types'] = "gif|jpg|png|jpeg";
         $this->CI->upload->initialize($config);

         if (!$this->CI->upload->do_upload('images'))
         {//if error
             $render['msg'] = $this->CI->upload->display_errors();
             $view_data['flash']['error'] = $this->CI->twig->render('admin/msg.html', $render);
         }
         else
         {//if not error do modif_image
             //create thumb
             $this->_create_thumb();

             //save image object
             $images_mdl = new Image();
             $images_mdl->save_to_images();
             $view_data['images'][] = $images_mdl->get_one();
             $view_data['flash'] = "<div class='alert-message success'> success </div>";
             //return html
             //change db_object to array;
             return $this->CI->twig->render('admin/_pics_collate.html', $view_data);
         }
     }

     /** resize and create thumb from uploaded files
      *
      * @param type $uploaded
      * @return array of [success] or [error] 
      */
     private function _create_thumb()
     {
         //init image_lib
         //set file_upload config
         //thumb config
         $uploaded = $this->CI->upload->data();
         $images_config['image_library']    = 'gd2';
         $images_config['create_thumb']     = TRUE;
         $images_config['source_image']     = $uploaded['full_path'];
         $images_config['new_image']        = $this->img_dir . $uploaded['file_name'];
         $images_config['quality']          = 100;



         $request_width = $this->CI->input->post('images_width');
         $request_height = $this->CI->input->post('images_height');

         //if both widht*height not empty dont scale on ratio;
         if ($request_width != "" && $request_height != "")
         {
             $images_config['maintain_ratio'] = FALSE;
             $images_config['width'] = $request_width;
             $images_config['height'] = $request_height;
         }
         else
         {
             $images_config['maintain_ratio'] = TRUE;

             //fix maintainratio
             //if width not empty set ratio for height
             if ($request_width)
             {
                 $ratio = $request_width / $uploaded['image_width'];

                 $images_config['width'] = $request_width;
                 $images_config['height'] = $ratio * $uploaded['image_height'];
             }

             //if height not empty set ratio for width
             if ($request_height)
             {
                 $ratio = $request_height / $uploaded['image_height'];
                 $images_config['height'] = $request_height;
                 $images_config['width'] = $ratio * $uploaded['image_width'];
             }


             //if width and height empty set default thumbnail
             if ($request_width == "" && $request_height == "")
             {
                 $images_config['height'] = '300';
                 $images_config['width'] = '300';
             }
         }//if request
         //fire::debug_message($images_config);
         $this->CI->load->library('image_lib', $images_config);

         if ($this->CI->image_lib->resize())
         {

             //if success add meta;
             $this->uploaded_file['thumb_name'] = $uploaded['raw_name'] . "_thumb" . $uploaded['file_ext'];

             return true;
         }

         return $return;
     }

     //put your code here
 }

 