<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Twig {

    private $CI;
    private $_twig;
    private $_template_dir;
    private $_cache_dir;

    /**
     * Constructor
     *
     */
    function __construct($debug = false) {
        $this->CI = & get_instance();
        $this->CI->config->load('twig');

        ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . APPPATH . 'libraries/Twig');
        require_once (string) "Autoloader" . EXT;

        log_message('debug', "Twig Autoloader Loaded");

        Twig_Autoloader::register();

        $this->_template_dir = $this->CI->config->item('template_dir');
        $this->_cache_dir = $this->CI->config->item('cache_dir');


        $loader = new Twig_Loader_Filesystem($this->_template_dir);

        $this->_twig = new Twig_Environment($loader, array(
                    'cache' => $this->_cache_dir,
                    'debug' => $debug,
                ));


        $this->_twig->addExtension(new Twig_Extension_Debug());


        //get helper
        $default_function = get_defined_functions();
        //fire::debug_message(get_defined_functions());
        foreach ($default_function['user'] as $function) {
            $this->_twig->addFunction($function, new Twig_Function_Function($function));
        }



        //load functions
        $this->_load_functions = $this->CI->config->item('load_functions');
        if ($this->_load_functions) {
            foreach ($this->_load_functions as $function) {
                $this->_twig->addFunction($function, new Twig_Function_Function($function));
            }
        }
    }

    public function add_function($name) {


        if (is_array($name)) {
            foreach ($name as $function) {
                $this->_twig->addFunction($function, new Twig_Function_Function($function));
            }
        } else {
            $this->_twig->addFunction($name, new Twig_Function_Function($name));
        }
    }

    public function render($template, $data = array()) {
        $template = $this->_twig->loadTemplate($template);
        return $template->render($data);
    }

    public function display($template, $data = array()) {

        $template = $this->_twig->loadTemplate($template);
        $template->display($data);
    }

}