<?php

 /*
  * To change this template, choose Tools | Templates
  * and open the template in the editor.
  */

 /**
  * Driver for produk
  */
 if(!defined('BASEPATH'))
     exit('No direct script access allowed');

 class Drv_post extends CI_Driver_Library
 {

     var $CI;

     function __construct()
     {
         $this->CI = & get_instance();
     }

     function save_post()
     {
         $post = $this->CI->input->post();
         $post_mdl = new Post();
         $post_mdl->id = $post['id'] ? $post['id'] : null;
         $post_mdl->post_date = timestamp_to_mysqldatetime();
         $post_mdl->post_title = $post['post_title'];
         $post_mdl->post_content = $post['post_content'];
         $post_mdl->post_type = $post['post_type'];
         $post_mdl->parentpost_id = $post['parentpost_id'];
         $post_mdl->publish = $post['publish'] ? $post['publish'] : " ";
         $post_mdl->save();

         if($post['meta'])
         {
             //loop meta[type] from html
             foreach($post['meta'] as $key => $value)
             {

                 //check if meta exits
                 $meta_mdl = $post_mdl->Post_meta->where('post_id', $post_mdl->id)->where('type', $key)->get();

                 //filter if $_post value not empty
                 if($value)
                 {
                     $meta_mdl->post_id = $post_mdl->id;
                     $meta_mdl->type = $key;
                     $meta_mdl->content = $value;
                     $meta_mdl->save();
                 }
             }
         }

         //checkbox 
         $checkbox_list = array('currentproject');

         foreach($checkbox_list as $key => $value)
         {
             //check if meta exits
             $meta_mdl = $post_mdl->Post_meta->where('post_id', $post_mdl->id)->where('type', $value)->get();
             $meta_mdl->post_id = $post_mdl->id;
             $meta_mdl->type = $value;
             $meta_mdl->content = $post[$value] ? $post[$value] : '';
             $meta_mdl->save();
         }


         $date_happen = $post['meta_date_happen'];
         if($date_happen['confirm'])
         {
             $meta_mdl = $post_mdl->Post_meta->where('post_id', $post_mdl->id)->where('type', 'date_happen')->get();

             $meta_date_happen = $post['meta_date_happen'];
             $mysqldate = $meta_date_happen['year'] . ":" . $meta_date_happen['month'] . ":" . $meta_date_happen['day'] . " " . $meta_date_happen['hour'] . ":" . $meta_date_happen['minute'] . ":00";

             $meta_mdl->post_id = $post_mdl->id;
             $meta_mdl->type = 'date_happen';
             $meta_mdl->content = $mysqldate;
             $meta_mdl->save();

             //fire::debug_message(array($mysqldate,$meta_date_happen,timestamp_to_mysqldatetime));
         }



         return true;
     }
     

     function test()
     {

         fire::debug_message($this->CI->input->post());
     }

     //put your code here
 }

 