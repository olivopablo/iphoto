<?php

class fire {

    function debug_message($message=null,$target='page') {
        $inspector = FirePHP::to($target);
        $console = $inspector->console();
        $console->log($message);
        $console->trace('label');
    }
    
       function debug_json($message=null,$assoc='false') {
        $inspector = FirePHP::to('page');
        $console = $inspector->console();
        $console->log(json_decode($message,$assoc));
    }

}